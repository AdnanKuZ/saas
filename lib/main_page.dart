import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/widgets/custom_appbar.dart';
import 'package:saas/app/widgets/custom_drawer.dart';
import 'package:saas/core/drawer_functions.dart';
import 'package:saas/core/main_page_service.dart';
import 'package:saas/features/home/presentation/home_page.dart';
import 'package:saas/features/profile/presentation/profile_page.dart';
import 'package:saas/features/settings/presentation/settings_page.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:saas/features/trips/my_trips/presentation/trips_page.dart';
import 'features/home/presentation/cubit/home_news_cubit.dart';
import 'features/home/presentation/cubit/home_slider_cubit.dart';
import 'features/notifications/presentation/cubit/notifications_cubit.dart';
class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  String _pageTitle = "Home";
  MainPageService pageService = DI.mainService;
  @override
  void initState() {
    super.initState();
    pageService.pageTitleCon.listen((title) {
      setState(() {
        _pageTitle = title;
      });
    });
    BlocProvider.of<HomeSliderCubit>(context).getSlider();
    BlocProvider.of<HomeNewsCubit>(context).getNews();
    BlocProvider.of<NotificationsCubit>(context).getNotifications();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size(MediaQuery.sizeOf(context).width, 55),
          child: CustomAppBar(_pageTitle)),
      drawer: const CustomDrawer(),
      body: PersistentTabView(
        controller: DI.mainService.pageController,
        context,
        screens: _buildScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.white, // Default is Colors.white.
        handleAndroidBackButtonPress: true, // Default is true.
        resizeToAvoidBottomInset:
            true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
        stateManagement: true, // Default is true.
        hideNavigationBarWhenKeyboardShows:
            true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
        decoration: NavBarDecoration(
            borderRadius: BorderRadius.circular(13.0),
            colorBehindNavBar: Colors.white,
            border:
                Border.all(color: AppColors.textFieldBorderColor, width: 0.5)),
        popAllScreensOnTapOfSelectedTab: true,
        popActionScreens: PopActionScreensType.all,
        itemAnimationProperties: const ItemAnimationProperties(
          // Navigation Bar's items animation properties.
          duration: Duration(milliseconds: 200),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: const ScreenTransitionAnimation(
          // Screen transition animation on change of selected tab.
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
        padding: const NavBarPadding.only(top: 6, bottom: 8), navBarHeight: 60,
        navBarStyle:
            NavBarStyle.style6, // Choose the nav bar style with this property.

        onItemSelected: (value) {
          changeTitle(value);
          if (value == 0) {
            DI.drawer.setSelectedMenuItem(MenuEnum.Home);
          }
        },
      ),
    );
  }

  List<Widget> _buildScreens() {
    return [
      const Home(),
      const TripsPage(),
      const ProfilePage(),
      const SettingsPage()
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
          icon:
              SizedBox(width: 21, child: Image.asset("assets/icons/home.png")),
          title: "Home",
          activeColorPrimary: CupertinoColors.systemRed,
          inactiveColorPrimary: AppColors.mainblue,
          iconSize: 100),
      PersistentBottomNavBarItem(
        icon: SizedBox(width: 32, child: Image.asset("assets/icons/plane.png")),
        title: "My Trips",
        activeColorPrimary: CupertinoColors.systemRed,
        inactiveColorPrimary: AppColors.mainblue,
      ),
      PersistentBottomNavBarItem(
        icon:
            SizedBox(width: 21, child: Image.asset("assets/icons/person.png")),
        title: "My Profile",
        activeColorPrimary: CupertinoColors.systemRed,
        inactiveColorPrimary: AppColors.mainblue,
      ),
      PersistentBottomNavBarItem(
        icon: SizedBox(
            width: 19, child: Image.asset("assets/icons/settings.png")),
        title: "Settings",
        activeColorPrimary: CupertinoColors.systemRed,
        inactiveColorPrimary: AppColors.mainblue,
      ),
    ];
  }

  changeTitle(int index) {
    switch (index) {
      case 0:
        {
          setState(() => _pageTitle = "Home");
        }
      case 1:
        {
          setState(() => _pageTitle = "My Trips");
        }
      case 2:
        {
          setState(() => _pageTitle = "My Profile");
        }
      case 3:
        {
          setState(() => _pageTitle = "Settings");
        }
      default:
        setState(() => _pageTitle = "Home");
    }
  }
}
