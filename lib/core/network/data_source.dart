import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/error/excpetions.dart';

Future<T> dataSource<T>(Function call,
    {Function? model, bool isUpdateUser = false, bool isAuth = false}) async {
  final response = await call();
  print("responseIs: ${response.body}");
  print("statusCodeIs: ${response.statusCode}");
  var jsonResponse = json.decode(response.body);
  if (response.statusCode == 200 && jsonResponse["data"] == null) {
    if (jsonResponse["error_description"] == "please login again") {
      DI.userService.logout();
      Fluttertoast.showToast(msg: "Session Expired");
      throw HttpException(jsonResponse["error_description"]);
    }
    throw HttpException(jsonResponse["error_description"]??"");
  } else if (response.statusCode == 401) {
    DI.userService.logout();
    Fluttertoast.showToast(msg: "Session Expired");
    throw HttpException(jsonResponse["error_description"]);
  } else if (response.statusCode == 200 && jsonResponse["data"] != null) {
    if (model == null) {
      return Future.value(null);
    }
    return isAuth ? model(response.body, false) : model(response.body);
  } else {
    throw jsonResponse["error_description"] == null
        ? HttpException("Error Occured")
        : HttpException(jsonResponse["error_description"]);
  }
}
