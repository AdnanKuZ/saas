import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:saas/features/home/presentation/home_page.dart';
import 'package:saas/features/profile/presentation/profile_page.dart';
import 'package:saas/features/statics/presentation/about_us_page.dart';
import 'package:saas/features/statics/presentation/contact_us_page.dart';

class DrawerUtils {
  BehaviorSubject<MenuState> MenuStatecontroller = BehaviorSubject<MenuState>();

  get MenuStateControllerStream => MenuStatecontroller.stream;

  void dispose() {
    MenuStatecontroller.close();
  }

  // Set menu state
  void setSelectedMenuItem(MenuEnum selectedMenuItem) {
    MenuState? state = getLastMenuStatus();
    if (state == null) {
      MenuStatecontroller.sink
          .add(MenuState(selectedMenuItem: selectedMenuItem));
    } else {
      state.selectedMenuItem = selectedMenuItem;
      MenuStatecontroller.sink.add(state);
    }
  }

  // Last menu state
  MenuState? getLastMenuStatus() {
    return MenuStatecontroller.valueOrNull;
  }

  // Initial menu state
  MenuState getInitialMenuState() {
    return MenuState(selectedMenuItem: MenuEnum.Home);
  }

  Widget RenderWidget(
    MenuEnum menu,
  ) {
    print(menu);
    switch (menu) {
      case MenuEnum.Home:
        return const HomePage();
      // case MenuEnum.MyTrip:
      //   return TripDetailPage(fromDrawer: true);
      case MenuEnum.MyProfile:
        return const ProfilePage();
      case MenuEnum.ContactUs:
        return  ContactUsPage();
      case MenuEnum.AboutUs:
        return const AboutUsPage();
      default:
        return const HomePage();
    }
  }
}

enum MenuEnum { Home, MyTrip, MyProfile, ContactUs, AboutUs }

class MenuState {
  MenuEnum selectedMenuItem;

  MenuState({required this.selectedMenuItem});
}
