import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:rxdart/rxdart.dart';

class MainPageService {
  PersistentTabController controller = PersistentTabController(initialIndex: 0);
  PersistentTabController get pageController => controller;

  BehaviorSubject<String> pageTitleCon = BehaviorSubject<String>();
void dispose() {
    pageTitleCon.close();
  }
  get pageTitle => pageTitleCon.stream;
  setPageTitle(String title) {
    pageTitleCon.sink.add(title);
  }
}
