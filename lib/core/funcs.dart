import 'package:saas/core/error/failures.dart';

String getErrorMessage(Failure error) {
  if (error is ServerFailure) {
    return 'Server Error Please Try Again Later';
  } else if (error is HttpFailure) {
    return error.message;
  } else {
    return 'Unknown Error Please Try Again Later';
  }
}

String getTextFieldValidationInfo(String email, String password,
    {bool isSignUp = false,
    String? userName,
    String? confirmPass,
    String? phone}) {
  String message = "";
  if (email.isEmpty) {
    message = "Email should Not Be Empty";
  }
  if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(email) &&
      email.isNotEmpty) {
    message = "$message\nInvalid email address format";
  }
  if (password.isEmpty) {
    message = "$message\nPassword should Not Be Empty";
  }
  if (password.length < 6 && password.isNotEmpty) {
    message = "$message\nPassword should not be less than 6 characters";
  }
  if (isSignUp) {
    if (confirmPass != password) {
      message = "$message\nPasswords do not match";
    }
    if (userName!.isEmpty) {
      message = "$message\nUsername should not be empty";
    }
    if (phone!.isEmpty) {
      message = "$message\nPhone should not be empty";
    }
    if (!RegExp(r'^((\+|00)?9639|0?9)([3-6]|[8,9])\d{7}$').hasMatch(phone) &&
        phone.isNotEmpty) {
      message = "$message\nPlease enter a syrian phone number";
    }
  }
  return message.trim();
}

String? emailValidator(String? value) {
  if (value == null || value.isEmpty) {
    return 'Please enter your email address.';
  }
  final emailRegex = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  if (!emailRegex.hasMatch(value)) {
    return 'Please enter a valid email address.';
  }
  return null;
}

String? passwordValidator(String? value) {
  if (value == null || value.isEmpty) {
    return 'Please enter your password.';
  }
  if (value.length < 8) {
    return 'Password must be at least 8 characters long.';
  }
  return null;
}

String format24to12(String? time) {
  //time string should be like this:"21:56:00"
  String hour = '';
  String minute = '';
  String aa = 'AM';
  if (time == null || time.isEmpty) {
    return "";
  }
  hour = time.split(':')[0]; //21
  minute = time.split(':')[1]; //56
  if (int.parse(hour) > 12) {
    hour = (int.parse(hour) - 12).toString();
    aa = 'PM';
  } else if (int.parse(hour) == 0) {
    hour = "12";
    aa = 'PM';
  }
  return "$hour:$minute $aa";
}
