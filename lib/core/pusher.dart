import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pusher_channels_flutter/pusher_channels_flutter.dart';
import 'package:saas/app/di.dart';

class PusherService {
  final eventFormKey = GlobalKey<FormState>();
  bool isConnected = false;
  PusherChannelsFlutter pusher = PusherChannelsFlutter.getInstance();
  Future<void> connect() async {
    try {
      await pusher.init(
        apiKey: "fe77102117b50e466174", //api key for the pusher account
        cluster: "eu", //cluster value
        onConnectionStateChange: onConnectionStateChange,
        onError: onError,
        onSubscriptionSucceeded: onSubscriptionSucceeded,
        onEvent: onEvent,
        onSubscriptionError: onSubscriptionError,
        onDecryptionFailure: onDecryptionFailure,
        onMemberAdded: onMemberAdded,
        onMemberRemoved: onMemberRemoved,
        onSubscriptionCount: onSubscriptionCount,
        // authEndpoint: "<Your Authendpoint Url>",
        // onAuthorizer: onAuthorizer
      );
      await pusher.subscribe(
          channelName:
              "notificationChannel_${DI.userService.getUser()!.user.id}"); //channel name either public or private here we create a channel for each user so it's a private channel and we add the user id
      await pusher.connect(); //connect to the subscribed channel
      isConnected = true;
    } catch (e) {
      print("ERROR: $e");
    }
  }

  void onTriggerEventPressed() async {
    var eventFormValidated = eventFormKey.currentState!.validate();

    if (!eventFormValidated) {
      return;
    }
    pusher.trigger(PusherEvent(
        channelName: "notificationChannel_${DI.userService.getUser()!.user.id}",
        eventName: "_eventName.text",
        data: "_data.text"));
  }

  void onConnectionStateChange(dynamic currentState, dynamic previousState) {
    print("Connection: $currentState");
  }

  void onError(String message, int? code, dynamic e) {
    print("onError: $message code: $code exception: $e");
  }

  Future<void> onEvent(PusherEvent event) async {
    print("onEvent: $event");
    if (event.data.isNotEmpty) {
      Map eventData = json.decode(event.data);
      await DI.localNotificationsService
          .showLocalNotification("Saas App", eventData["message"]);
    }
  }

  void onSubscriptionSucceeded(String channelName, dynamic data) {
    print("onSubscriptionSucceeded: $channelName data: $data");
    final me = pusher.getChannel(channelName)?.me;
    print("Me: $me");
  }

  void onSubscriptionError(String message, dynamic e) {
    print("onSubscriptionError: $message Exception: $e");
  }

  void onDecryptionFailure(String event, String reason) {
    print("onDecryptionFailure: $event reason: $reason");
  }

  void onMemberAdded(String channelName, PusherMember member) {
    print("onMemberAdded: $channelName user: $member");
  }

  void onMemberRemoved(String channelName, PusherMember member) {
    print("onMemberRemoved: $channelName user: $member");
  }

  void onSubscriptionCount(String channelName, int subscriptionCount) {
    print(
        "onSubscriptionCount: $channelName subscriptionCount: $subscriptionCount");
  }
}
