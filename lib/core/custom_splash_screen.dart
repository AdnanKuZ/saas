import 'dart:async';
import 'package:flutter/material.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/page_transition.dart';
import 'package:saas/features/authentication/presentation/login_page.dart';
import 'package:saas/features/welcome/welcome_page.dart';
import 'package:saas/main_page.dart';

class MyCustomSplashScreen extends StatefulWidget {
  const MyCustomSplashScreen({super.key});

  @override
  State<MyCustomSplashScreen> createState() => _MyCustomSplashScreenState();
}

class _MyCustomSplashScreenState extends State<MyCustomSplashScreen>
    with TickerProviderStateMixin {
  double _containerSize = 1.5;
  double _containerOpacity = 0.0;

  late AnimationController _controller;
  late Animation<double> animation1;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 3));

    animation1 = Tween<double>(begin: 40, end: 20).animate(CurvedAnimation(
        parent: _controller, curve: Curves.fastLinearToSlowEaseIn))
      ..addListener(() {});

    _controller.forward();

    Timer(const Duration(seconds: 2), () {
      setState(() {
        _containerSize = 2;
        _containerOpacity = 1;
      });
    });

    Timer(const Duration(seconds: 4), () {
      setState(() {
        Navigator.pushReplacement(
            context,
            DI.welcomeService.getIsFirstTime() == null
                ? WelcomePageTransition(const WelcomePage())
                : DI.userService.getUser() == null
                    ? WelcomePageTransition(const LoginPage())
                    : WelcomePageTransition(const MainPage()));
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.sizeOf(context).width;

    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: MediaQuery.sizeOf(context).width,
              height: MediaQuery.sizeOf(context).height,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/splash_background.png"),
                      fit: BoxFit.fill)),
            ),
            Center(
              child: AnimatedOpacity(
                duration: const Duration(milliseconds: 2000),
                curve: Curves.fastLinearToSlowEaseIn,
                opacity: _containerOpacity,
                child: AnimatedContainer(
                    duration: const Duration(milliseconds: 2000),
                    curve: Curves.fastLinearToSlowEaseIn,
                    height: width / _containerSize,
                    width: width / _containerSize,
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/splash_logo.png')),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
