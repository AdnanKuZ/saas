import 'package:http/http.dart';
import 'package:saas/core/drawer_functions.dart';
import 'package:saas/features/notifications/service/local_notifications.dart';
import 'package:saas/core/main_page_service.dart';
import 'package:saas/core/pusher.dart';
import 'package:saas/features/authentication/data/auth_datasource.dart';
import 'package:saas/features/authentication/data/auth_repositories.dart';
import 'package:saas/features/authentication/presentation/cubit/auth_cubit.dart';
import 'package:saas/features/authentication/services/user_service.dart';
import 'package:saas/features/home/presentation/cubit/home_news_cubit.dart';
import 'package:saas/features/home/presentation/cubit/home_slider_cubit.dart';
import 'package:saas/features/notifications/data/notifications_datasource.dart';
import 'package:saas/features/notifications/data/notifications_repositories.dart';
import 'package:saas/features/notifications/presentation/cubit/notifications_cubit.dart';
import 'package:saas/features/profile/data/profile_datasource.dart';
import 'package:saas/features/profile/data/profile_repositories.dart';
import 'package:saas/features/profile/presentation/cubit/profile_cubit.dart';
import 'package:saas/features/statics/data/static_pages_datasource.dart';
import 'package:saas/features/statics/data/static_pages_repositories.dart';
import 'package:saas/features/statics/presentation/cubit/about_us_cubit.dart';
import 'package:saas/features/statics/presentation/cubit/contact_us_cubit.dart';
import 'package:saas/features/trips/my_trips/data/my_trips_datasource.dart';
import 'package:saas/features/trips/my_trips/data/my_trips_repositories.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_cubit.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_history_cubit.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_datasource.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_repositories.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/external_regions_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/internal_regions_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/post_trip_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/regions_cubit.dart';
import 'package:saas/features/trips/new_trip/services/new_trip_page_service.dart';
import 'package:saas/features/welcome/cubit/boarding_pages_cubit.dart';
import 'package:saas/features/welcome/services/welcome_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get_it/get_it.dart';

abstract class DI {
  static GetIt get di => GetIt.instance;

  static Future<void> init() async {
    final preferences = await SharedPreferences.getInstance();
    di.registerLazySingleton<WelcomeService>(() => WelcomeService(preferences));
    di.registerLazySingleton<UserService>(() => UserService(preferences));
    di.registerLazySingleton<PusherService>(() => PusherService());
    di.registerLazySingleton<LocalNotificationsService>(
        () => LocalNotificationsService());
    di.registerLazySingleton<DrawerUtils>(() => DrawerUtils());
    di.registerLazySingleton<NewTripPageService>(() => NewTripPageService());
    di.registerLazySingleton<MainPageService>(() => MainPageService());
    di.registerLazySingleton<Client>(() => Client());
    registerAuth();
    registerNewTrip();
    registerMyTrips();
    registerCustomer();
    registerStaticPages();
    registerNotifications();
  }

  static void registerAuth() async {
    di.registerLazySingleton<AuthDataSource>(
        () => AuthDataSource(di<Client>()));
    di.registerLazySingleton<AuthRepositories>(
        () => AuthRepositories(di<AuthDataSource>()));
    di.registerFactory<AuthCubit>(() => AuthCubit(di<AuthRepositories>()));
  }

  static void registerNewTrip() async {
    di.registerLazySingleton<NewTripDataSource>(
        () => NewTripDataSource(di<Client>()));
    di.registerLazySingleton<NewTripRepositories>(
        () => NewTripRepositories(di<NewTripDataSource>()));

    di.registerFactory<RegionsCubit>(
        () => RegionsCubit(di<NewTripRepositories>()));
    di.registerFactory<InternalRegionsCubit>(
        () => InternalRegionsCubit(di<NewTripRepositories>()));
    di.registerFactory<ExternalRegionsCubit>(
        () => ExternalRegionsCubit(di<NewTripRepositories>()));
    di.registerFactory<PostTripCubit>(
        () => PostTripCubit(di<NewTripRepositories>()));
  }

  static void registerMyTrips() async {
    di.registerLazySingleton<MyTripsDataSource>(
        () => MyTripsDataSource(di<Client>()));
    di.registerLazySingleton<MyTripsRepositories>(
        () => MyTripsRepositories(di<MyTripsDataSource>()));

    di.registerFactory<MyTripsHistoryCubit>(
        () => MyTripsHistoryCubit(di<MyTripsRepositories>()));

    di.registerFactory<MyTripsCubit>(
        () => MyTripsCubit(di<MyTripsRepositories>()));
  }

  static void registerCustomer() async {
    di.registerLazySingleton<ProfileDataSource>(
        () => ProfileDataSource(di<Client>()));
    di.registerLazySingleton<ProfileRepositories>(
        () => ProfileRepositories(di<ProfileDataSource>()));
    di.registerFactory<ProfileCubit>(
        () => ProfileCubit(di<ProfileRepositories>()));
  }

  static void registerStaticPages() async {
    di.registerLazySingleton<StaticPagesDataSource>(
        () => StaticPagesDataSource(di<Client>()));
    di.registerLazySingleton<StaticPagesRepositories>(
        () => StaticPagesRepositories(di<StaticPagesDataSource>()));
    di.registerFactory<AboutUsCubit>(
        () => AboutUsCubit(di<StaticPagesRepositories>()));
    di.registerFactory<ContactUsCubit>(
        () => ContactUsCubit(di<StaticPagesRepositories>()));
    di.registerFactory<HomeSliderCubit>(
        () => HomeSliderCubit(di<StaticPagesRepositories>()));
    di.registerFactory<HomeNewsCubit>(
        () => HomeNewsCubit(di<StaticPagesRepositories>()));
    di.registerFactory<BoardingPagesCubit>(
        () => BoardingPagesCubit(di<StaticPagesRepositories>()));
  }

  static void registerNotifications() async {
    di.registerLazySingleton<NotificationsDataSource>(
        () => NotificationsDataSource(di<Client>()));
    di.registerLazySingleton<NotificationsRepositories>(
        () => NotificationsRepositories(di<NotificationsDataSource>()));
    di.registerFactory<NotificationsCubit>(
        () => NotificationsCubit(di<NotificationsRepositories>()));
  }

  static UserService get userService => di.get<UserService>();
  static PusherService get pusherService => di.get<PusherService>();
  static LocalNotificationsService get localNotificationsService =>
      di.get<LocalNotificationsService>();
  static WelcomeService get welcomeService => di.get<WelcomeService>();
  static DrawerUtils get drawer => di.get<DrawerUtils>();
  static NewTripPageService get newTripPageService =>
      di.get<NewTripPageService>();
  static MainPageService get mainService => di.get<MainPageService>();
}
