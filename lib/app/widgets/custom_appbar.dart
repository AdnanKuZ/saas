import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/features/notifications/presentation/widgets/notifications_bell_widget.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar(this.text, {super.key, this.tripDetail = false});
  final String text;
  final bool tripDetail;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      centerTitle: true,
      title: Text(
        tripDetail ? "Trip No :  $text" : text,
        style: TextStyles.pageTitleTextStyle,
      ),
      leading: Builder(
        builder: (context) {
          return IconButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            icon: Image.asset(
              "assets/icons/drawer_icons/drawer.png",
            ),
          );
        },
      ),
      actions: const [NotificationsBellWidget()],
    );
  }
}
