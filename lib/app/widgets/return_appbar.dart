import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';

class CustomAppBarWithReturn extends StatelessWidget {
  const CustomAppBarWithReturn(
      {super.key,
      this.redTitle = false,
      this.title,
      this.tripDetail = false,
      this.onReturn,
      this.textWidget});
  final String? title;
  final bool tripDetail;
  final Widget? textWidget;
  final bool redTitle;
  final VoidCallback? onReturn;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Padding(
          padding: EdgeInsets.only(right: 40),
          child: SizedBox(),
        ),
        textWidget ??
            Text(
              tripDetail ? "Trip No :  $title" : title ?? "Login",
              style: redTitle
                  ? TextStyles.pageTitleRedTextStyle
                  : TextStyles.pageTitleTextStyle,
            ),
        Padding(
          padding: const EdgeInsets.only(right: 18),
          child: InkWell(
            onTap: () {
              if (onReturn != null) {
                onReturn!();
                Navigator.pop(context);
              } else {
                Navigator.pop(context);
              }
            },
            child: const Icon(CupertinoIcons.arrow_right),
          ),
        ),
      ],
    );
  }
}
