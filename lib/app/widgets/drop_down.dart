import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';

class CustomDropDownWidget<T> extends StatelessWidget {
  const CustomDropDownWidget(
      {super.key,
      required this.value,
      required this.dropDownList,
      required this.onChanged,
      this.borderRadius,
      this.fillColor,
      this.hintWidget,
      this.border});
  final List<DropdownMenuItem<T>>? dropDownList;
  final T value;
  final void Function(T? value) onChanged;
  final double? borderRadius;
  final Color? fillColor;
  final Widget? hintWidget;
  final InputBorder? border;
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
        isExpanded: true,
        iconSize: 42,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 25),
            borderSide: const BorderSide(color: AppColors.mainblue),
          ),
          border: border ??
              OutlineInputBorder(
                borderRadius: BorderRadius.circular(borderRadius ?? 25),
                borderSide: const BorderSide(color: AppColors.mainblue),
              ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(borderRadius ?? 25),
              borderSide: const BorderSide(color: AppColors.mainblue)),
          fillColor: fillColor ?? Colors.white,
          filled: true,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        ),
        hint: hintWidget,
        value: value,
        onChanged: onChanged,
        style: const TextStyle(color: Colors.black, fontSize: 18),
        items: dropDownList);
  }
}
