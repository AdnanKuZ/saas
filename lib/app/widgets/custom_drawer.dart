import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/drawer_functions.dart';
import 'package:saas/features/authentication/data/user_model.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    UserModel user = DI.userService.getUser()!;
    return Container(
      height: MediaQuery.sizeOf(context).height * 0.83,
      width: MediaQuery.sizeOf(context).width * 0.83,
      padding: const EdgeInsets.fromLTRB(20, 14, 34, 38),
      decoration: const BoxDecoration(
          color: AppColors.mainblue,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(54), bottomRight: Radius.circular(54))),
      child: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
                onTap: () => Navigator.pop(context),
                child:
                    Image.asset('assets/icons/drawer_icons/drawer_opened.png')),
          ),
          const SizedBox(height: 10),
          CircleAvatar(
              radius: 35, child: Image.asset("assets/images/avatar.png")),
          const SizedBox(height: 10),
          Text(
            user.user.name,
            style: TextStyles.drawerNameTextStyle,
          ),
          const SizedBox(height: 10),
          Text(user.user.email, style: TextStyles.drawerGreyTextStyle),
          const SizedBox(height: 80),
          ListTile(
            leading: Image.asset('assets/icons/drawer_icons/home.png'),
            horizontalTitleGap: 3,
            title: Text("Home", style: TextStyles.drawerTilesTextStyle),
            onTap: () => pushScreen(context, MenuEnum.Home),
          ),
          // ListTile(
          //   leading: Image.asset('assets/icons/drawer_icons/my_trip.png'),
          //   horizontalTitleGap: 3,
          //   title: Text("My Trip", style: TextStyles.drawerTilesTextStyle),
          //   onTap: () => pushScreen(context, MenuEnum.MyTrip,pageTitle: "My Trip"),
          // ),
          ListTile(
            leading: Image.asset('assets/icons/drawer_icons/my_profile.png'),
            horizontalTitleGap: 3,
            title: Text("My Profile", style: TextStyles.drawerTilesTextStyle),
            onTap: () => pushScreen(context, MenuEnum.MyProfile,
                pageTitle: "My Profile"),
          ),

          ListTile(
            leading: Image.asset('assets/icons/drawer_icons/contact_us.png'),
            horizontalTitleGap: 3,
            title: Text("Contact Us", style: TextStyles.drawerTilesTextStyle),
            onTap: () => pushScreen(context, MenuEnum.ContactUs,
                pageTitle: "Contact Us"),
          ),
          ListTile(
            leading: Image.asset('assets/icons/drawer_icons/about_us.png'),
            horizontalTitleGap: 3,
            title: Text("About Us", style: TextStyles.drawerTilesTextStyle),
            onTap: () =>
                pushScreen(context, MenuEnum.AboutUs, pageTitle: "About Us"),
          ),
          SizedBox(height: MediaQuery.sizeOf(context).height * 0.046),
          TextButton(
              onPressed: () async => await DI.userService.logout(),
              child: Text(
                "Log Out",
                style: TextStyles.drawerGreyTextStyle,
              )),
        ],
      )),
    );
  }

  void pushScreen(BuildContext context, MenuEnum page, {String? pageTitle}) {
    Scaffold.of(context).closeDrawer();
    DI.mainService.pageController.jumpToTab(0);
    DI.mainService.setPageTitle(pageTitle ?? "Home");
    DI.drawer.setSelectedMenuItem(page);
  }
}
