import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField(
      {Key? key,
      this.isObscur,
      this.onFieldSubmit,
      this.onSaved,
      this.suffix,
      this.prefix,
      this.textInputAction = TextInputAction.done,
      this.validator,
      this.fillColor = Colors.white,
      this.textColor = Colors.black,
      this.hintColor = Colors.black,
      this.controller,
      this.fontSize = 14,
      this.keyboardType = TextInputType.emailAddress,
      this.padding,
      this.hintText = '',
      this.textDirection = TextDirection.ltr,
      this.borderRadius,
      this.inputFormatters,
      this.border,
      this.enabledBorder,
      this.disabledBorder,
      this.errorBorder,
      this.textStyle,
      this.borderSide,
      this.disabledBorderSide,
      this.enabledBorderSide,
      this.errorBorderSide,
      this.onChanged})
      : super(key: key);
  final void Function(String?)? onSaved;
  final String? Function(String?)? validator;
  final void Function(String?)? onFieldSubmit;
  final TextInputAction textInputAction;
  final bool? isObscur;
  final TextEditingController? controller;
  final Widget? suffix;
  final Widget? prefix;
  final Color fillColor;
  final TextInputType keyboardType;
  final Color textColor;
  final TextStyle? textStyle;
  final Color hintColor;
  final double fontSize;
  final EdgeInsetsGeometry? padding;
  final String hintText;
  final TextDirection textDirection;
  final double? borderRadius;
  final List<TextInputFormatter>? inputFormatters;
  final BorderSide? borderSide;
  final InputBorder? border;
  final BorderSide? enabledBorderSide;
  final InputBorder? enabledBorder;
  final BorderSide? errorBorderSide;
  final InputBorder? errorBorder;
  final BorderSide? disabledBorderSide;
  final InputBorder? disabledBorder;
  final Function(String text)? onChanged;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlign: TextAlign.left,
      controller: controller,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
          errorStyle: const TextStyle(height: 0),
          border: border ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: borderSide ?? BorderSide.none),
          enabledBorder: enabledBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: enabledBorderSide ?? BorderSide.none),
          disabledBorder: disabledBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: disabledBorderSide ?? BorderSide.none),
          errorBorder: errorBorder ??
              OutlineInputBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(borderRadius ?? 15.0)),
                  borderSide: errorBorderSide ??
                      const BorderSide(
                        color: Color(0xFFF01738),
                      )),
          hintText: hintText,
          contentPadding: padding ??
              const EdgeInsets.symmetric(horizontal: 14, vertical: 8),
          filled: true,
          prefixIcon: prefix,
          fillColor: fillColor,
          suffixIcon: suffix,
          hintTextDirection: TextDirection.rtl,
          hintStyle: TextStyle(
            color: hintColor,
          )
          // prefix: prefix,
          ),
      obscuringCharacter: '*',
      style: textStyle ??
          TextStyle(
              letterSpacing: (isObscur != null && isObscur!) ? 3 : 0,
              color: textColor,
              fontSize: fontSize),
      textDirection: textDirection,
      obscureText: isObscur ?? false,
      keyboardType: keyboardType,
      textInputAction: textInputAction,
      validator: validator,
      onChanged: onChanged,
      onSaved: onSaved,
      onFieldSubmitted: onFieldSubmit,
    );
  }
}
