import "package:flutter/material.dart";
import 'package:saas/app/constants/colors.dart';

abstract class TextStyles {
  //Bold fontWeight: FontWeight.w700,
  //Semi Bold fontWeight: FontWeight.w600,
  //Medium
  static TextStyle newsWidgetTextStyle = const TextStyle(
    color: Color(0xFF302D2C),
    fontSize: 14,
    fontFamily: "Poppins",
  );

  static TextStyle pageTitleTextStyle = const TextStyle(
    color: AppColors.mainblue,
    fontFamily: "Montserrat",
    fontWeight: FontWeight.w500,
    fontSize: 19,
  );
  static TextStyle pageTitleRedTextStyle = const TextStyle(
    color: Color(0xFFF01738),
    fontFamily: "Montserrat",
    fontWeight: FontWeight.w500,
    fontSize: 19,
  );
  static TextStyle pageBlueTextStyle = const TextStyle(
    color: AppColors.mainblue,
    fontFamily: "Montserrat",
    fontWeight: FontWeight.w500,
    fontSize: 16,
  );
  static TextStyle drawerNameTextStyle = const TextStyle(
    color: Colors.white,
    fontFamily: "Montserrat",
    fontWeight: FontWeight.w500,
    fontSize: 21,
  );
  static TextStyle secondaryPageTitleTextStyle = const TextStyle(
    color: AppColors.mainblue,
    fontFamily: "Chillax",
    fontWeight: FontWeight.w500,
    fontSize: 21,
  );
  static TextStyle welcomePageTextStyle1 = const TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: "Chillax",
    fontSize: 24,
  );
  static TextStyle boardingWidgetTextStyle = const TextStyle(
    color: AppColors.textColor,
    fontWeight: FontWeight.w500,
    fontFamily: "Chillax",
    fontSize: 18,
  );
  static TextStyle resendCodeTextStyle = const TextStyle(
    color: Color(0xFFF01738),
    fontWeight: FontWeight.w500,
    fontFamily: "Chillax",
    fontSize: 16,
  );
  static TextStyle drawerTilesTextStyle = const TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: "Chillax",
    fontSize: 16,
  );

  static TextStyle drawerGreyTextStyle = const TextStyle(
    color: Colors.grey,
    fontWeight: FontWeight.w500,
    fontFamily: "Montserrat",
    fontSize: 14,
  );
  static TextStyle cancelTripDialogTitleTextStyle = const TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: "Chillax",
    fontSize: 16,
  );
  static TextStyle notificationWidgetTitleTextStyle = const TextStyle(
    color: AppColors.mainblue,
    fontFamily: "Chillax",
    fontWeight: FontWeight.w500,
    fontSize: 15,
  );
  static TextStyle notificationWidgetDateTextStyle = const TextStyle(
    color: Color(0xFF8F9BB3),
    fontFamily: "NunitoSans",
    fontWeight: FontWeight.w600,
    fontSize: 10,
  );
  //Regular
  static TextStyle cancelTripDialogBodyTextStyle = const TextStyle(
    color: Colors.black,
    fontFamily: "Chillax",
    fontSize: 14,
  );
  static TextStyle tripMediumTextStyle = const TextStyle(
    color: Colors.black,
    fontSize: 15,
  );
  static TextStyle profileTilesTextStyle = const TextStyle(
    color: Colors.white,
    fontFamily: "Chillax",
    fontSize: 16,
  );
  static TextStyle welcomePageTextStyle2 = const TextStyle(
    color: Colors.white,
    fontFamily: "Chillax",
    fontSize: 16,
  );
  static TextStyle policyTextStyle = const TextStyle(
    color: AppColors.textColor,
    fontFamily: "NunitoSans",
    fontSize: 11,
  );
  static TextStyle textFieldTextStyle = const TextStyle(
    color: AppColors.textColor,
    fontFamily: "Montserrat",
    fontSize: 17,
  );
  static TextStyle selectTripTypeTextStyle = const TextStyle(
    color: Color(0xFFF01738),
    fontFamily: "Chillax",
    fontSize: 14,
  );
  //Light
  static TextStyle lightTextStyle = const TextStyle(
    color: AppColors.textColor,
    fontSize: 19,
    fontWeight: FontWeight.w300,
  );

  static TextStyle smallLightTextStyle = const TextStyle(
    color: Color(0xFFA29F9E),
    fontFamily: "Chillax",
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );
  static TextStyle tripLightTextStyle = const TextStyle(
    color: Color(0xFF93928E),
    fontFamily: "Chillax",
    fontSize: 12,
  );
}
