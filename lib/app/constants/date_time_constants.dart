abstract class DateTimeConstants {
  static const dateFormat = 'yyyy/MM/dd';
  static const dateFormat2 = 'yyyy-MM-dd';
  static const dateFormat3 = 'd MMMM yyyy';
  static const dateFormat3WithoutYear = 'd MMMM';
  static const timeFormat = 'hh:mm a';
  static const dateTimeFormat = '$dateFormat|$timeFormat';
}
