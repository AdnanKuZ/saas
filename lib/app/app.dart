import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:saas/core/custom_splash_screen.dart';
import 'package:saas/app/di.dart';
import 'package:saas/features/authentication/presentation/cubit/auth_cubit.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:saas/features/home/presentation/cubit/home_news_cubit.dart';
import 'package:saas/features/home/presentation/cubit/home_slider_cubit.dart';
import 'package:saas/features/notifications/presentation/cubit/notifications_cubit.dart';
import 'package:saas/features/profile/presentation/cubit/profile_cubit.dart';
import 'package:saas/features/statics/presentation/cubit/about_us_cubit.dart';
import 'package:saas/features/statics/presentation/cubit/contact_us_cubit.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_cubit.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_history_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/external_regions_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/internal_regions_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/post_trip_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/regions_cubit.dart';
import 'package:saas/features/welcome/cubit/boarding_pages_cubit.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:timeago/timeago.dart' as timeago;

// part 'app_localization.dart';
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class SaasApp extends StatelessWidget {
  static Future<void> init() async {
    //splash screen
    FlutterNativeSplash.preserve(
        widgetsBinding: WidgetsFlutterBinding.ensureInitialized());
    await initLocalization();
    await DI.init();
    DI.localNotificationsService.initLocalNotifications();
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    Future.delayed(
      const Duration(milliseconds: 50),
      () => FlutterNativeSplash.remove(),
    );
  }

  static Future<void> initLocalization() async {
    WidgetsFlutterBinding.ensureInitialized();
    // await EasyLocalization.ensureInitialized();
    initializeDateFormatting();
  }

  const SaasApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => DI.di<AuthCubit>()),
          BlocProvider(create: (_) => DI.di<RegionsCubit>()),
          BlocProvider(create: (_) => DI.di<InternalRegionsCubit>()),
          BlocProvider(create: (_) => DI.di<ExternalRegionsCubit>()),
          BlocProvider(create: (_) => DI.di<PostTripCubit>()),
          BlocProvider(create: (_) => DI.di<MyTripsHistoryCubit>()),
          BlocProvider(create: (_) => DI.di<MyTripsCubit>()),
          BlocProvider(create: (_) => DI.di<ProfileCubit>()),
          BlocProvider(create: (_) => DI.di<AboutUsCubit>()),
          BlocProvider(create: (_) => DI.di<ContactUsCubit>()),
          BlocProvider(create: (_) => DI.di<BoardingPagesCubit>()),
          BlocProvider(create: (_) => DI.di<HomeSliderCubit>()),
          BlocProvider(create: (_) => DI.di<HomeNewsCubit>()),
          BlocProvider(create: (_) => DI.di<NotificationsCubit>()),
        ],
        child: MaterialApp(
            // localizationsDelegates: context.localizationDelegates,
            // supportedLocales: context.supportedLocales,
            // locale: context.locale,
            debugShowCheckedModeBanner: false,
            navigatorKey: navigatorKey,
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: const MyCustomSplashScreen()));
  }
}
