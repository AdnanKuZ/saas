import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/features/statics/presentation/leadership_message_page.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22),
        child: Column(
          children: [
            const SizedBox(height: 36),
            _settingsRow("assets/icons/languages.png", "Language",
                Text("English", style: TextStyles.resendCodeTextStyle)),
            const SizedBox(height: 26),
            _settingsRow(
                "assets/icons/terms_conditions.png",
                "Terms and Conditions",
                InkWell(
                  onTap: () {},
                  child: const Icon(
                    CupertinoIcons.chevron_forward,
                    color: Color(0xFFF01738),
                  ),
                )),
            const SizedBox(height: 26),
            _settingsRow(
                "assets/icons/rate_app.png",
                "Rate the App",
                InkWell(
                  onTap: () {},
                  child: const Icon(
                    CupertinoIcons.chevron_forward,
                    color: Color(0xFFF01738),
                  ),
                )),
            const SizedBox(height: 26),
            _settingsRow(
                "assets/icons/profile.png",
                "Leadership Message",
                InkWell(
                  onTap: () {
                    pushNewScreen(context, screen: const LeadershipMessagePage(),withNavBar: false);
                  },
                  child: const Icon(
                    CupertinoIcons.chevron_forward,
                    color: Color(0xFFF01738),
                  ),
                )),
          ],
        ),
      )),
    );
  }

  Widget _settingsRow(String image, String title, Widget suffix) => SizedBox(
        width: double.infinity,
        child: Row(
          children: [
            SizedBox(
              width: 23,
              child: Image.asset(image),
            ),
            const SizedBox(width: 20),
            Text(
              title,
              style: const TextStyle(
                color: AppColors.mainblue,
                fontSize: 17,
              ),
            ),
            const Spacer(),
            suffix
          ],
        ),
      );
}
