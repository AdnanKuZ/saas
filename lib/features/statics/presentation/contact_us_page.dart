import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/app/widgets/text_form_field.dart';
import 'package:saas/features/statics/presentation/cubit/contact_us_cubit.dart';

class ContactUsPage extends StatelessWidget {
  ContactUsPage({super.key});
  final TextEditingController nameCon = TextEditingController();
  final TextEditingController mobileCon = TextEditingController();
  final TextEditingController subjectCon = TextEditingController();
  final TextEditingController messageCon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Form(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                  child: Text("Welcome Back!",
                      style: TextStyles.boardingWidgetTextStyle)),
              const SizedBox(height: 16),
              const Text(
                "You can contact us by\nphone\ne-mail\n\nOr by filling out the form below",
                style: TextStyle(color: AppColors.mainblue),
              ),
              const SizedBox(height: 26),
              Text("Your Name", style: TextStyles.smallLightTextStyle),
              _TextFiled(controller: nameCon),
              const SizedBox(height: 26),
              Text("Mobile", style: TextStyles.smallLightTextStyle),
              _TextFiled(controller: mobileCon),
              const SizedBox(height: 26),
              Text("Subject", style: TextStyles.smallLightTextStyle),
              _TextFiled(controller: subjectCon),
              const SizedBox(height: 26),
              Text("Message", style: TextStyles.smallLightTextStyle),
              _TextFiled(controller: messageCon),
              const SizedBox(height: 26),
              Row(
                children: [
                  Expanded(
                    child: CustomElevatedButton(
                      title: "Submit",
                      buttonColor: AppColors.mainblue,
                      borderRadius: 8,
                      verticalPadding: 15,
                      onPressed: () {
                        Map<String, dynamic> data = {
                          "name": nameCon.text,
                          "mobile": mobileCon.text,
                          "subject": subjectCon.text,
                          "note": messageCon.text
                        };
                        BlocProvider.of<ContactUsCubit>(context)
                            .posrContactUs(data);
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 56),
            ],
          ),
        ),
      )),
    );
  }
}

class _TextFiled extends StatelessWidget {
  const _TextFiled({required this.controller});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return CustomTextFormField(
      controller: controller,
      border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: AppColors.textFieldBorderColor,
            width: 2,
          )),
      enabledBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        borderSide: BorderSide(
          color: AppColors.textFieldBorderColor,
          width: 2,
        ),
      ),
      disabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide.none),
      errorBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: Color(0xFFF01738),
          )),
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 15),
      fillColor: Colors.white,
      validator: (value) {
        if (value!.isEmpty) {
          return "some field are empty";
        }
        return null;
      },
    );
  }
}
