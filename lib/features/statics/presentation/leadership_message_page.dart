import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/widgets/return_appbar.dart';

class LeadershipMessagePage extends StatelessWidget {
  const LeadershipMessagePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
            child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          width: MediaQuery.sizeOf(context).width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 18),
              const CustomAppBarWithReturn(title: "Leadership Message"),
              const SizedBox(height: 18),
              Image.asset('assets/images/leadership_message.png'),
              const SizedBox(height: 16),
              const Text(
                "“Our culture in Saudi Amad Co. “SAAS” is built on the values that we believe in and work for every day. We strive to achieve results, haste to accomplish, engage our clients, aspire with no limitations to succeed, and take pride in being authentic and innovative. These values are our credo in serving our clients, employees, and beloved country. We are committed to crafting a unique mobility experience for our clients through our modern fleet. We promise our clients to provide creative mobility solutions and a pleasant experience on every trip. We are proud to be the pioneers of introducing environmentally friendly fleet in Saudi airports, which aligns with the Saudi vision 2030 of reducing carbon emissions and achieving net zero, to be truly an “ICON OF MOBILITY”.",
                style: TextStyle(
                    color: AppColors.mainblue, fontSize: 16, height: 1.5),
                textAlign: TextAlign.justify,
              ),
              const SizedBox(height: 36),
            ],
          ),
        )),
      ),
    );
  }
}
