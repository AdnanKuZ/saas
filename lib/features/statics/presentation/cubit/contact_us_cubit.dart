import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/statics/data/static_pages_repositories.dart';

part 'contact_us_state.dart';

class ContactUsCubit extends Cubit<ContactUsState> {
  final StaticPagesRepositories _staticPagesRepositories;
  ContactUsCubit(this._staticPagesRepositories) : super(ContactUsInitial());
  Future<void> posrContactUs(Map<String, dynamic> data) async {
    print(data);
    emit(LoadingState());
    final either = await _staticPagesRepositories.postContactUs(data);
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(PostedContactUsState());
    });
  }
}
