part of 'contact_us_cubit.dart';

abstract class ContactUsState {
  const ContactUsState();
}

class ContactUsInitial extends ContactUsState {}

class LoadingState extends ContactUsState {}

class PostedContactUsState extends ContactUsState {
  PostedContactUsState();
}

class ErrorState extends ContactUsState {
  final String message;

  ErrorState(this.message);
}
