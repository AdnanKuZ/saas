import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';
import 'package:saas/features/statics/data/static_pages_repositories.dart';

part 'about_us_state.dart';

class AboutUsCubit extends Cubit<AboutUsState> {
  final StaticPagesRepositories _staticPagesRepositories;
  AboutUsCubit(this._staticPagesRepositories)
      : super(AboutUsInitial());
  Future<void> getAboutUS() async {
    emit(LoadingState());
    final either = await _staticPagesRepositories.getAboutUs();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotAboutUsState(data));
    });
  }
}
