part of 'about_us_cubit.dart';

abstract class AboutUsState {
  const AboutUsState();
}

class AboutUsInitial extends AboutUsState {}

class LoadingState extends AboutUsState {}

class GotAboutUsState extends AboutUsState {
  final StaticPagesModel model;

  GotAboutUsState(this.model);
  
}

class ErrorState extends AboutUsState {
  final String message;

  ErrorState(this.message);
}
