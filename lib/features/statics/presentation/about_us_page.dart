import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';
import 'package:saas/features/statics/presentation/cubit/about_us_cubit.dart';

class AboutUsPage extends StatelessWidget {
  const AboutUsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
            child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          width: MediaQuery.sizeOf(context).width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 16),
              BlocConsumer<AboutUsCubit, AboutUsState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is LoadingState) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  if (state is GotAboutUsState) {
                    return _MidSection(
                      data: state.model.data[0],
                    );
                  }
                  return const _MidSection();
                },
              ),
              const SizedBox(height: 36),
            ],
          ),
        )),
      ),
    );
  }
}

class _MidSection extends StatelessWidget {
  const _MidSection({this.data});
  final Datum? data;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        data == null
            ? Image.asset('assets/images/about_us.png')
            : Image.network(
                "$imageUrl${data!.image}",
                loadingBuilder: (context, child, loadingProgress) =>
                    Image.asset('assets/images/about_us.png'),
              ),
        const SizedBox(height: 16),
        Text(
          data == null
              ? "SAAS is a company that specializes in mobility solutions inside and outside airports for local and international companies using modern environment-friendly vehicles. The company is running a successful business since 1987 until today together with a talented team that strives to make every trip a memorable experience for our clients"
              : data!.text ?? "",
          style: const TextStyle(
              color: AppColors.mainblue, fontSize: 16, height: 1.5),
          textAlign: TextAlign.justify,
        ),
      ],
    );
  }
}
