import 'dart:convert';

import 'package:http/http.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/network/data_source.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';

class StaticPagesDataSource {
  final Client _client;
  StaticPagesDataSource(this._client);

  Future<StaticPagesModel> getAboutUs() async => _request(aboutUsUrl);
  Future<StaticPagesModel> getHomeSlider() async => _request(homeSliderUrl);
  Future<StaticPagesModel> getHomeNews() async => _request(homeNewsUrl);
  Future<StaticPagesModel> getWelcomePages() async => _request(welcomePagesUrl);
  
  Future<void> postContactUs(Map<String, dynamic> data) async => dataSource(
        () => _client.post(
          Uri.parse(contactUsUrl),
          body: json.encode(data),
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer ${DI.userService.getUser()?.token}"
          },
        ),
      );

  Future<StaticPagesModel> _request<StaticPagesModel>(String url) async =>
      dataSource(
        () => _client.post(
          Uri.parse(url),
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer ${DI.userService.getUser()?.token}"
          },
        ),
        model: staticPagesModelFromJson,
      );
}
