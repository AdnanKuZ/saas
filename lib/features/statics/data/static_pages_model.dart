import 'dart:convert';

StaticPagesModel staticPagesModelFromJson(String str) =>
    StaticPagesModel.fromJson(json.decode(str)["data"]);

String staticPagesModelToJson(StaticPagesModel data) =>
    json.encode(data.toJson());

class StaticPagesModel {
  int? currentPage;
  List<Datum> data;
  int? from;
  int? lastPage;
  int? perPage;
  int? to;
  int? total;

  StaticPagesModel({
    this.currentPage,
    required this.data,
    this.from,
    this.lastPage,
    this.perPage,
    this.to,
    this.total,
  });

  factory StaticPagesModel.fromJson(Map<String, dynamic> json) =>
      StaticPagesModel(
        currentPage: json["current_page"],
        data: json["data"] == null
            ? []
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        from: json["from"],
        lastPage: json["last_page"],
        perPage: json["per_page"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "from": from,
        "last_page": lastPage,
        "per_page": perPage,
        "to": to,
        "total": total,
      };
  List<String> getListOfDatumImages() {
    List<String> imageList = [];
    for (var staticPageModel in data) {
      imageList.add(staticPageModel.image ?? "");
    }
    return imageList;
  }
}

class Datum {
  int id;
  String? title;
  String? text;
  String? image;
  DateTime? createdAt;
  DateTime? updatedAt;

  Datum({
    required this.id,
    this.title,
    this.text,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        title: json["title"],
        text: json["text"],
        image: json["image"],
        createdAt: json["created_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "text": text,
        "image": image,
        "created_at": createdAt == null
            ? DateTime.now().toIso8601String()
            : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null
            ? DateTime.now().toIso8601String()
            : updatedAt!.toIso8601String(),
      };
}
