import 'package:dartz/dartz.dart';
import 'package:saas/core/error/failures.dart';
import 'package:saas/core/network/repositories.dart';
import 'package:saas/features/statics/data/static_pages_datasource.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';

class StaticPagesRepositories {
  final StaticPagesDataSource _staticPagesDataSource;
  StaticPagesRepositories(this._staticPagesDataSource);

  Future<Either<Failure, StaticPagesModel>> getAboutUs() async =>
      await repository(() async => await _staticPagesDataSource.getAboutUs());

  Future<Either<Failure, StaticPagesModel>> getHomeSlider() async =>
      await repository(
          () async => await _staticPagesDataSource.getHomeSlider());

  Future<Either<Failure, StaticPagesModel>> getHomeNews() async =>
      await repository(() async => await _staticPagesDataSource.getHomeNews());

  Future<Either<Failure, StaticPagesModel>> getWelcomePages() async =>
      await repository(
          () async => await _staticPagesDataSource.getWelcomePages());
          
  Future<Either<Failure, void>> postContactUs(
          Map<String, dynamic> data) async =>
      await repository(
          () async => await _staticPagesDataSource.postContactUs(data));
}
