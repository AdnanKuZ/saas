// To parse this JSON data, do
//
//     final internalRegionModel = internalRegionModelFromJson(jsonString);

import 'dart:convert';

List<InternalRegionModel> internalRegionModelListFromJson(String str) =>
    List<InternalRegionModel>.from(
        json.decode(str)["data"].map((x) => InternalRegionModel.fromJson(x)));

String internalRegionModelListToJson(List<InternalRegionModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

InternalRegionModel internalRegionModelFromJson(String str) =>
    InternalRegionModel.fromJson(json.decode(str)["data"]);

String internalRegionModelToJson(InternalRegionModel data) =>
    json.encode(data.toJson());

class InternalRegionModel {
  String id;
  String buildingId;
  String name;
  String name2;

  InternalRegionModel({
    required this.id,
    required this.buildingId,
    required this.name,
    required this.name2,
  });

  factory InternalRegionModel.fromJson(Map<String, dynamic> json) =>
      InternalRegionModel(
        id: json["id"],
        buildingId: json["building_id"],
        name: json["name"],
        name2: json["name_2"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "building_id": buildingId,
        "name": name,
        "name_2": name2,
      };
}
