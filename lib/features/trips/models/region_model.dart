import 'dart:convert';

List<RegionModel> regionModelListFromJson(String str) => List<RegionModel>.from(
    json.decode(str)["data"].map((x) => RegionModel.fromJson(x)));

String regionModelListToJson(List<RegionModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

RegionModel regionModelFromJson(String str) =>
    RegionModel.fromJson(json.decode(str)["data"]);

String regionModelToJson(RegionModel data) => json.encode(data.toJson());

class RegionModel {
  int id;
  String parent;
  int? parentCode;
  String lang;
  String cityId;
  String? type;
  String name;
  String? longitude;
  String? latitude;
  int? to;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? cityName;

  RegionModel({
    required this.id,
    required this.parent,
    this.parentCode,
    required this.lang,
    required this.cityId,
    this.type,
    required this.name,
    this.longitude,
    this.latitude,
    this.to,
    this.createdAt,
    this.updatedAt,
    this.cityName,
  });

  factory RegionModel.fromJson(Map<String, dynamic> json) => RegionModel(
        id: json["id"],
        parent: json["parent"],
        parentCode: json["parent_code"],
        lang: json["lang"],
        cityId: json["city_id"],
        type: json["type"],
        name: json["name"],
        longitude: json["longitude"],
        latitude: json["Latitude"],
        to: json["to"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        cityName: json["city_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "parent": parent,
        "parent_code": parentCode,
        "lang": lang,
        "city_id": cityId,
        "type": type,
        "name": name,
        "longitude": longitude,
        "Latitude": latitude,
        "to": to,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "city_name": cityName,
      };
  
}
