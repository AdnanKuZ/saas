import 'dart:convert';

TripModel tripModelFromJson(String str) =>
    TripModel.fromJson(json.decode(str)["data"]);

String tripModelToJson(TripModel data) => json.encode(data.toJson());

class TripModel {
  int currentPage;
  List<Trip> data;
  int? from;
  int lastPage;
  int? to;
  int total;
  TripModel({
    required this.currentPage,
    required this.data,
    this.from,
    required this.lastPage,
    this.to,
    required this.total,
  });
  factory TripModel.fromJson(Map<String, dynamic> json) => TripModel(
        currentPage: json["current_page"],
        data: json["data"].isNotEmpty
            ? List<Trip>.from(json["data"].map((x) => Trip.fromJson(x)))
            : [],
        from: json["from"],
        lastPage: json["last_page"],
        to: json["to"],
        total: json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "from": from,
        "last_page": lastPage,
        "to": to,
        "total": total,
      };
}

class Trip {
  int id;
  int tripId;
  String? from;
  String? to;
  dynamic otherTo;
  dynamic driverId;
  dynamic carsId;
  String? timeIn;
  dynamic start;
  dynamic end;
  int? status;
  dynamic reason;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? date;
  int? type;
  dynamic note;
  int? tripCityId;
  String? fromRegion;
  String? fromGroup;
  String? fromBuilding;
  String? toGroup;
  String? toBuilding;
  String? toRegion;
  Driver? driver;
  Vehicle? vehicles;
  int? cancel;

  Trip({
    required this.id,
    required this.tripId,
    this.from,
    this.to,
    this.otherTo,
    this.driverId,
    this.carsId,
    this.timeIn,
    this.start,
    this.end,
    this.status,
    this.reason,
    this.createdAt,
    this.updatedAt,
    this.date,
    this.type,
    this.note,
    this.tripCityId,
    this.fromRegion,
    this.fromGroup,
    this.fromBuilding,
    this.toGroup,
    this.toBuilding,
    this.toRegion,
    this.driver,
    this.vehicles,
    this.cancel,
  });

  factory Trip.fromJson(Map<String, dynamic> json) => Trip(
        id: json["id"],
        tripId: json["trip_id"],
        from: json["from"],
        to: json["to"],
        otherTo: json["other_to"],
        driverId: json["driver_id"],
        carsId: json["cars_id"],
        timeIn: json["time_in"],
        start: json["start"],
        end: json["end"],
        status: json["status"],
        reason: json["reason"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        date: DateTime.parse(json["date"]),
        type: json["type"],
        note: json["note"],
        tripCityId: json["trip_city_id"],
        fromRegion: json["from_region"],
        fromGroup: json["from_region"],
        fromBuilding: json["from_building"],
        toGroup: json["to_group"],
        toBuilding: json["to_building"],
        toRegion: json["to_region"],
        driver: json["driver"] == null ? null : Driver.fromJson(json["driver"]),
        vehicles: json["vehicles"] == null ? null : Vehicle.fromJson(json["vehicles"]),
        cancel: json["cancel"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "trip_id": tripId,
        "from": from,
        "to": to,
        "other_to": otherTo,
        "driver_id": driverId,
        "cars_id": carsId,
        "time_in": timeIn,
        "start": start,
        "end": end,
        "status": status,
        "reason": reason,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "date":
            "${date?.year.toString().padLeft(4, '0')}-${date?.month.toString().padLeft(2, '0')}-${date?.day.toString().padLeft(2, '0')}",
        "type": type,
        "note": note,
        "trip_city_id": tripCityId,
        "from_region": fromRegion,
        "from_group": fromGroup,
        "from_building": fromBuilding,
        "to_group": toGroup,
        "to_building": toBuilding,
        "to_region": toRegion,
        "driver": driver == null ? null : driver!.toJson(),
        "vehicles": vehicles,
        "cancel": cancel,
      };
  String getType() {
    switch (type) {
      case 1:
        return "Internal";
      case 2:
        return "External";
      case 3:
        return "Other";
      default:
        return "Internal";
    }
  }
}

class Driver {
  int id;
  String name;

  Driver({
    required this.id,
    required this.name,
  });

  factory Driver.fromJson(Map<String, dynamic> json) => Driver(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}

class Vehicle {
  int id;
  String licensePlate;

  Vehicle({
    required this.id,
    required this.licensePlate,
  });

  factory Vehicle.fromJson(Map<String, dynamic> json) => Vehicle(
        id: json["id"],
        licensePlate: json["license_plate"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "license_plate": licensePlate,
      };
}
