import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/my_trips/data/my_trips_repositories.dart';

part 'my_trips_history_state.dart';

class MyTripsHistoryCubit extends Cubit<MyTripsHistoryState> {
  final MyTripsRepositories _myTripsRepositories;
  MyTripsHistoryCubit(this._myTripsRepositories)
      : super(MyTripsHistoryInitial());
  Future<void> getTripHistory() async {
    emit(LoadingState());
    final either = await _myTripsRepositories.getTripHistory();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotTripsHistoryState(data));
    });
  }
}
