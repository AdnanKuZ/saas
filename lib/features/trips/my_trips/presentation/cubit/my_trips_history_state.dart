part of 'my_trips_history_cubit.dart';

abstract class MyTripsHistoryState {
  const MyTripsHistoryState();
}

class MyTripsHistoryInitial extends MyTripsHistoryState {}

class LoadingState extends MyTripsHistoryState {}

class GotTripsHistoryState extends MyTripsHistoryState {
  final TripModel trips;

  GotTripsHistoryState(this.trips);
  
}

class ErrorState extends MyTripsHistoryState {
  final String message;

  ErrorState(this.message);
}
