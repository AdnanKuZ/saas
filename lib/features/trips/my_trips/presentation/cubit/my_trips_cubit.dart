import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/my_trips/data/my_trips_repositories.dart';

part 'my_trips_state.dart';

class MyTripsCubit extends Cubit<MyTripsState> {
  final MyTripsRepositories _myTripsRepositories;
  MyTripsCubit(this._myTripsRepositories) : super(MyTripsInitial());
  Future<void> getMyTrips() async {
    emit(LoadingState());
    final either = await _myTripsRepositories.getMyTrips();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotTripsState(data));
    });
  }
}
