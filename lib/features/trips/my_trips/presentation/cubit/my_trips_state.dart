part of 'my_trips_cubit.dart';

abstract class MyTripsState {
  const MyTripsState();
}

class MyTripsInitial extends MyTripsState {}

class LoadingState extends MyTripsState {}

class GotTripsState extends MyTripsState {
  final TripModel trips;

  GotTripsState(this.trips);
  
}

class ErrorState extends MyTripsState {
  final String message;

  ErrorState(this.message);
}
