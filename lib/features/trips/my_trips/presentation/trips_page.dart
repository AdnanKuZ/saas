import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/custom_drawer.dart';
import 'package:saas/features/home/presentation/widgets/add_trip_button.dart';

import 'my_trips_tab.dart';
import 'trip_history_tab.dart';

class TripsPage extends StatefulWidget {
  const TripsPage({super.key});

  @override
  State<TripsPage> createState() => _TripsPageState();
}

class _TripsPageState extends State<TripsPage> {
  bool isTrips = true;
  late PageController _controller;
  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      drawer: const CustomDrawer(),
      floatingActionButton: const AddTripButton(width: 50),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              const SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: _SwitchSection(
                  select: _select,
                  isTrips: isTrips,
                ),
              ),
              const SizedBox(height: 12),
              Container(
                height: MediaQuery.sizeOf(context).height * 0.72,
                padding: const EdgeInsets.symmetric(horizontal: 12),
                color: Colors.grey.shade50,
                child: PageView(
                  controller: _controller,
                  physics: const NeverScrollableScrollPhysics(),
                  children: const [
                    MyTripsTab(),
                    TripsHistoryTab(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _select() {
    _controller.animateToPage(isTrips ? 1 : 0,
        duration: const Duration(milliseconds: 150), curve: Curves.bounceIn);
    setState(
      () {
        isTrips = !isTrips;
      },
    );
  }
}

class _SwitchSection extends StatefulWidget {
  const _SwitchSection({required this.select, required this.isTrips});
  final Function() select;
  final bool isTrips;
  @override
  State<_SwitchSection> createState() => _SwitchSectionState();
}

class _SwitchSectionState extends State<_SwitchSection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.sizeOf(context).width,
      height: 50,
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          color: AppColors.mainblue, borderRadius: BorderRadius.circular(14)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: widget.select,
              child: AnimatedContainer(
                height: 40,
                padding: const EdgeInsets.all(6),
                duration: const Duration(milliseconds: 200),
                decoration: BoxDecoration(
                    color: widget.isTrips ? Colors.white : AppColors.mainblue,
                    borderRadius: BorderRadius.circular(14)),
                child: Center(
                    child: Text(
                  "My Trips",
                  style: widget.isTrips
                      ? TextStyles.resendCodeTextStyle
                      : TextStyles.drawerTilesTextStyle,
                )),
              ),
            ),
          ),
          const SizedBox(width: 4),
          Expanded(
            child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: widget.select,
              child: AnimatedContainer(
                height: 40,
                padding: const EdgeInsets.all(6),
                duration: const Duration(milliseconds: 200),
                decoration: BoxDecoration(
                    color: !widget.isTrips ? Colors.white : AppColors.mainblue,
                    borderRadius: BorderRadius.circular(14)),
                child: Center(
                    child: Text(
                  "History",
                  style: !widget.isTrips
                      ? TextStyles.resendCodeTextStyle
                      : TextStyles.drawerTilesTextStyle,
                )),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
