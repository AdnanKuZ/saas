import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/extensions/date_time_helper.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/trip_detail/presentation/trip_detail_page.dart';

import '../../../../../core/funcs.dart';

class TripWidget extends StatelessWidget {
  const TripWidget(this.trip, {super.key});
  final Trip trip;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => pushNewScreen(context,
          screen: TripDetailPage(trip: trip), withNavBar: false),
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(12)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 12, top: 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Trip No:  ${trip.tripId}",
                    style: const TextStyle(
                        color: AppColors.textColor, fontSize: 11),
                  ),
                  Text(
                    trip.getType(),
                    style: const TextStyle(color: Color(0xFFF01738), fontSize: 11),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 4),
            const Divider(),
            const SizedBox(height: 4),
            MyTripsPickUpDropOffWidget(trip),
            const SizedBox(height: 4),
            const Divider(),
            const SizedBox(height: 4),
            SizedBox(
              width: double.infinity,
              child: Wrap(
                alignment: WrapAlignment.start,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  const SizedBox(width: 8),
                  SizedBox(
                      width: 20,
                      child: Image.asset('assets/icons/calendar.png')),
                  const SizedBox(width: 8),
                  Text(
                    trip.date?.formattedDate3 ?? "",
                    style: const TextStyle(
                        color: AppColors.textColor, fontSize: 11),
                  ),
                  const SizedBox(width: 26),
                  SizedBox(
                      width: 20, child: Image.asset('assets/icons/clock.png')),
                  const SizedBox(width: 8),
                  Text(
                    format24to12(trip.timeIn),
                    style: const TextStyle(
                        color: AppColors.textColor, fontSize: 11),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyTripsPickUpDropOffWidget extends StatelessWidget {
  const MyTripsPickUpDropOffWidget(this.trip, {super.key});
  final Trip trip;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            SizedBox(width: 33, child: Image.asset('assets/icons/pin.png')),
            const SizedBox(height: 22, child: VerticalDivider()),
            SizedBox(width: 33, child: Image.asset('assets/icons/flag.png')),
          ],
        ),
        const SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Pick-up",
              style: TextStyles.tripLightTextStyle,
            ),
            Text(
              "${trip.fromGroup}\nBuilding: ${trip.fromBuilding}",
              style: TextStyles.tripMediumTextStyle,
            ),
            const SizedBox(height: 18),
            Text(
              "Drop-off",
              style: TextStyles.tripLightTextStyle,
            ),
            Text(
              trip.getType() == "Internal"
                  ? "${trip.toGroup}\nBuilding: ${trip.toBuilding}"
                  : trip.getType() == "External"
                      ? trip.toRegion??""
                      : trip.otherTo??"",
              style: TextStyles.tripMediumTextStyle,
            ),
          ],
        ),
      ],
    );
  }
}
