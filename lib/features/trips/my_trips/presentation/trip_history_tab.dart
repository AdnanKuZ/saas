import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/widgets/error_occured_widget.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_history_cubit.dart';
import 'package:saas/features/trips/my_trips/presentation/widgets/trip_widget.dart';
import 'package:saas/app/widgets/shimmer.dart';

class TripsHistoryTab extends StatefulWidget {
  const TripsHistoryTab({super.key});

  @override
  State<TripsHistoryTab> createState() => _TripsHistoryTabState();
}

class _TripsHistoryTabState extends State<TripsHistoryTab> {
  late MyTripsHistoryCubit tripHistoryCubit;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tripHistoryCubit = BlocProvider.of<MyTripsHistoryCubit>(context);
    tripHistoryCubit.getTripHistory();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MyTripsHistoryCubit, MyTripsHistoryState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is LoadingState) {
            return shimmer(const ShimmerWidget());
          } else if (state is GotTripsHistoryState) {
            List<Trip> trips = state.trips.data;
            if (trips.isEmpty) {
              return const Center(
                  child: ErrorOccuredTextWidget(
                      errorType: ErrorType.empty, fun: null));
            } else {
              return RefreshIndicator(
                onRefresh: () => tripHistoryCubit.getTripHistory(),
                child: ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) => TripWidget(trips[index]),
                    separatorBuilder: (context, index) => const SizedBox(
                          height: 20,
                        ),
                    itemCount: trips.length),
              );
            }
          }
          return Center(
              child: ErrorOccuredTextWidget(
                  errorType: ErrorType.server,
                  fun: () => tripHistoryCubit.getTripHistory()));
        });
  }
}
