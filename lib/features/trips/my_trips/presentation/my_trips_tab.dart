import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/widgets/error_occured_widget.dart';
import 'package:saas/app/widgets/shimmer.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_cubit.dart';
import 'package:saas/features/trips/my_trips/presentation/widgets/trip_widget.dart';

class MyTripsTab extends StatefulWidget {
  const MyTripsTab({super.key});

  @override
  State<MyTripsTab> createState() => _MyTripsTabState();
}

class _MyTripsTabState extends State<MyTripsTab> {
  late MyTripsCubit myTripsCubit;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myTripsCubit = BlocProvider.of<MyTripsCubit>(context);
    myTripsCubit.getMyTrips();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MyTripsCubit, MyTripsState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is LoadingState) {
            return shimmer(const ShimmerWidget());
          } else if (state is GotTripsState) {
            List<Trip> trips = state.trips.data;
            if (trips.isEmpty) {
              return const Center(
                  child: ErrorOccuredTextWidget(
                      errorType: ErrorType.empty, fun: null));
            } else {
              return RefreshIndicator(
                onRefresh: () => myTripsCubit.getMyTrips(),
                child: ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) => TripWidget(trips[index]),
                    separatorBuilder: (context, index) => const SizedBox(
                          height: 20,
                        ),
                    itemCount: trips.length),
              );
            }
          }
          return Center(
              child: ErrorOccuredTextWidget(
                  errorType: ErrorType.server,
                  fun: () => myTripsCubit.getMyTrips()));
        });
  }
}
