import 'package:http/http.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/network/data_source.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/trips/models/trip_model.dart';

class MyTripsDataSource {
  final Client _client;
  MyTripsDataSource(this._client);
  Future<TripModel> getMyTrips() async {
    return dataSource(
      () => _client.post(
        Uri.parse(myTripsUrl),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}"
        },
      ),
      model: tripModelFromJson,
    );
  }

  Future<TripModel> getTripHistory() async {
    return dataSource(
      () => _client.post(
        Uri.parse(tripHistoryUrl),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}"
        },
      ),
      model: tripModelFromJson,
    );
  }
}
