import 'package:dartz/dartz.dart';
import 'package:saas/core/error/failures.dart';
import 'package:saas/core/network/repositories.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/my_trips/data/my_trips_datasource.dart';

class MyTripsRepositories {
  final MyTripsDataSource _myTripsDataSource;
  MyTripsRepositories(this._myTripsDataSource);
  Future<Either<Failure, TripModel>> getMyTrips() async =>
      await repository(() async => await _myTripsDataSource.getMyTrips());
  Future<Either<Failure, TripModel>> getTripHistory() async =>
      await repository(() async => await _myTripsDataSource.getTripHistory());
}
