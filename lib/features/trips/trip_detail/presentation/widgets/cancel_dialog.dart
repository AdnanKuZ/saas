import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/button.dart';

class CancelDialog extends StatelessWidget {
  const CancelDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.all(18),
        child: SizedBox(
          height: MediaQuery.sizeOf(context).height * 0.3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 10),
              Text(
                "Cancel Trip",
                style: TextStyles.resendCodeTextStyle,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              Text(
                "Are you sure to cancel the trip?",
                textAlign: TextAlign.center,
                style: TextStyles.cancelTripDialogTitleTextStyle,
              ),
              const SizedBox(height: 10),
              Text(
                "After canceling the trip we will take some of your time to evaluate the service",
                style: TextStyles.cancelTripDialogBodyTextStyle,
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: MediaQuery.sizeOf(context).width * 0.25, 
                    child: CustomElevatedButton(
                      title: "Yes",
                      buttonColor: Color(0xFFF01738),
                      borderRadius: 8,
                      verticalPadding: 0,
                      fontSize: 16,
                      onPressed: () {},
                    ),
                  ),
                  const SizedBox(width: 32),
                  SizedBox(
                    width: MediaQuery.sizeOf(context).width * 0.25,
                    child: CustomElevatedButton(
                      title: "No",
                      buttonColor: Color(0xFFF01738),
                      borderRadius: 8,
                      fontSize: 16,
                      verticalPadding: 0,
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
