import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/features/trips/models/trip_model.dart';

class DriverWidget extends StatelessWidget {
  const DriverWidget(this.driver, {super.key});
  final Driver driver;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 70,
          height:70,
          decoration: const BoxDecoration(
              color: AppColors.mainblue, shape: BoxShape.circle),
        ),
        const SizedBox(width: 8),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              driver.id.toString(),
              style: TextStyles.boardingWidgetTextStyle,
            ),
            const SizedBox(height: 8),
            Text(driver.name, style: TextStyles.drawerGreyTextStyle),
          ],
        ),
        const Spacer(),
        SizedBox(
          width: 35,
          child: Image.asset('assets/icons/telephone.png'),
        ),
        const SizedBox(width: 10),
        SizedBox(
          width: 35,
          child: Image.asset('assets/icons/chat.png'),
        )
      ],
    );
  }
}
