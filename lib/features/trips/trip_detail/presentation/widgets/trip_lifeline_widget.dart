import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';

class TripLifeLineWidget extends StatelessWidget {
  const TripLifeLineWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.sizeOf(context).width * 0.9,
      child: Row(
        children: [
          _circleContainer(const Color.fromRGBO(22, 64, 114, 1), 0),
          Expanded(child: _lineWidget(AppColors.textFieldBorderColor)),
          _circleContainer(Color(0xFFF01738), 1),
          Expanded(child: _lineWidget(AppColors.textFieldBorderColor)),
          _endCircleContainer()
        ],
      ),
    );
  }
}

Widget _circleContainer(Color color, int index) => Column(
      children: [
        Container(
          width: 30,
          height: 30,
          decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              border: Border.all(color: color, width: 10)),
        ),
        const SizedBox(height: 4),
        Text(index == 0 ? "Request" : "Arrived")
      ],
    );
Widget _endCircleContainer() => Column(
      children: [
        Container(
          width: 30,
          height: 30,
          decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              border:
                  Border.all(color: AppColors.textFieldBorderColor, width: 2)),
        ),
        const SizedBox(height: 4),
        const Text("End Trip")
      ],
    );
Widget _lineWidget(Color color) => Padding(
      padding: const EdgeInsets.only(bottom: 14),
      child: Divider(
        thickness: 2,
        color: color,
      ),
    );
