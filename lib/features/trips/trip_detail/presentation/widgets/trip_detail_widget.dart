import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/extensions/date_time_helper.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/core/funcs.dart';

class TripDetailWidget extends StatelessWidget {
  const TripDetailWidget(this.trip, {super.key});
  final Trip trip;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: Wrap(
            alignment: WrapAlignment.start,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              const SizedBox(width: 8),
              SizedBox(
                  width: 20, child: Image.asset('assets/icons/calendar.png')),
              const SizedBox(width: 8),
              Text(
                trip.date?.formattedDate3 ?? "",
                style:
                    const TextStyle(color: AppColors.textColor, fontSize: 11),
              ),
              const SizedBox(width: 26),
              SizedBox(width: 20, child: Image.asset('assets/icons/clock.png')),
              const SizedBox(width: 8),
              Text(
                format24to12(trip.timeIn),
                style:
                    const TextStyle(color: AppColors.textColor, fontSize: 11),
              ),
              const SizedBox(width: 66),
              Text(
                trip.getType(),
                style: const TextStyle(color: Color(0xFFF01738), fontSize: 11),
              ),
            ],
          ),
        ),
        const SizedBox(height: 10),
        const Divider(),
        const SizedBox(height: 10),
        TripDetailPickUpDropOffWidget(trip),
        const SizedBox(height: 20),
        const Divider(),
      ],
    );
  }
}

class TripDetailPickUpDropOffWidget extends StatelessWidget {
  const TripDetailPickUpDropOffWidget(this.trip, {super.key});
  final Trip trip;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            SizedBox(width: 30, child: Image.asset('assets/icons/pin.png')),
            DottedLine(
              direction: Axis.vertical,
              alignment: WrapAlignment.center,
              lineLength: 60,
              lineThickness: 1.0,
              dashLength: 5.0,
              dashColor: Colors.black,
              dashGradient: [Colors.grey.shade300, Colors.grey.shade300],
              dashRadius: 0.0,
              dashGapLength: 4.0,
              dashGapColor: Colors.white,
              dashGapGradient: const [Colors.white, Colors.white],
              dashGapRadius: 0.0,
            ),
            SizedBox(width: 30, child: Image.asset('assets/icons/flag.png')),
          ],
        ),
        const SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Pick-up",
              style: TextStyles.tripLightTextStyle,
            ),
            const SizedBox(height: 2),
            SizedBox(
              width: MediaQuery.sizeOf(context).width * 0.7,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    trip.from ?? "",
                    style: TextStyles.tripMediumTextStyle,
                  ),
                  Row(
                    children: [
                      SizedBox(
                          width: 18,
                          child: Image.asset('assets/icons/clock.png')),
                      const SizedBox(width: 6),
                      Text(
                        format24to12(trip.timeIn),
                        style: const TextStyle(
                            color: AppColors.textColor, fontSize: 11),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 58),
            Text(
              "Drop-off",
              style: TextStyles.tripLightTextStyle,
            ),
            const SizedBox(height: 2),
            SizedBox(
              width: MediaQuery.sizeOf(context).width * 0.7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    trip.getType() == "Other" ? trip.otherTo : trip.to ?? "",
                    style: TextStyles.tripMediumTextStyle,
                  ),
                  Row(
                    children: [
                      SizedBox(
                          width: 18,
                          child: Image.asset('assets/icons/clock.png')),
                      const SizedBox(width: 6),
                      Text(
                        format24to12(trip.timeIn),
                        style: const TextStyle(
                            color: AppColors.textColor, fontSize: 11),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
