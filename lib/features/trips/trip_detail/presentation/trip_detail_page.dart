import 'package:flutter/material.dart';
import 'package:saas/app/extensions/dialog_build_context.dart';
import 'package:saas/app/widgets/return_appbar.dart';

import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/features/trips/models/trip_model.dart';
import 'package:saas/features/trips/trip_detail/presentation/widgets/cancel_dialog.dart';
import 'package:saas/features/trips/trip_detail/presentation/widgets/driver_widget.dart';
import 'package:saas/features/trips/trip_detail/presentation/widgets/trip_detail_widget.dart';
import 'package:saas/features/trips/trip_detail/presentation/widgets/trip_lifeline_widget.dart';

class TripDetailPage extends StatelessWidget {
  const TripDetailPage({super.key, required this.trip});
  // final bool fromDrawer;
  final Trip trip;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 18),
          CustomAppBarWithReturn(
            textWidget: Row(children: [
              Text(
                "Trip No :  ",
                style: TextStyles.pageTitleTextStyle,
              ),
              Text(
                trip.id.toString(),
                style: TextStyles.resendCodeTextStyle,
              )
            ]),
            tripDetail: true,
          ),
          const SizedBox(height: 20),
          Center(
              child:
                  Text("Trip Details", style: TextStyles.resendCodeTextStyle)),
          const SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TripDetailWidget(trip),
                const SizedBox(height: 6),
                trip.driver == null
                    ? const SizedBox.shrink()
                    : _DriverSction(trip.driver!),
                // trip.vehicles == null
                //     ? const SizedBox.shrink()
                //     : const _TripSction(),
                const _TripSction(),
                const SizedBox(height: 36),
                Row(
                  children: [
                    Expanded(
                      child: CustomElevatedButton(
                        title: "Track",
                        buttonColor: AppColors.mainblue,
                        borderRadius: 8,
                        verticalPadding: 15,
                        onPressed: () {},
                      ),
                    ),
                    const SizedBox(width: 22),
                    Expanded(
                      child: CustomElevatedButton(
                        title: "Cancel",
                        buttonColor: AppColors.mainblue,
                        borderRadius: 8,
                        verticalPadding: 15,
                        onPressed: () {
                          context.showDialog(CancelDialog());
                        },
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 42),
              ],
            ),
          )
        ],
      ))),
    );
  }
}

class _DriverSction extends StatelessWidget {
  const _DriverSction(this.driver, {super.key});
  final Driver driver;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("Driver Details", style: TextStyles.resendCodeTextStyle),
        const SizedBox(height: 26),
        DriverWidget(driver),
        const SizedBox(height: 26),
        const Divider(),
        const SizedBox(height: 16),
      ],
    );
  }
}

class _TripSction extends StatelessWidget {
  const _TripSction({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("On the Way", style: TextStyles.boardingWidgetTextStyle),
        const SizedBox(height: 8),
        Text("3 minutes estimation to End",
            style: TextStyles.drawerGreyTextStyle),
        const SizedBox(height: 26),
        const TripLifeLineWidget(),
      ],
    );
  }
}
