import 'package:dartz/dartz.dart';
import 'package:saas/core/error/failures.dart';
import 'package:saas/core/network/repositories.dart';
import 'package:saas/features/trips/models/internal_region_model.dart';
import 'package:saas/features/trips/models/region_model.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_datasource.dart';

class NewTripRepositories {
  final NewTripDataSource _newTripDataSource;
  NewTripRepositories(this._newTripDataSource);

  Future<Either<Failure, List<RegionModel>>> getRegions() async =>
      await repository(() async => await _newTripDataSource.getRegions());

  Future<Either<Failure, List<InternalRegionModel>>> getInternalRegions(
          String regionParent) async =>
      await repository(() async =>
          await _newTripDataSource.getInternalRegions(regionParent));

  Future<Either<Failure, List<RegionModel>>> getExternalRegions() async =>
      await repository(
          () async => await _newTripDataSource.getExternalRegions());

  Future<Either<Failure, void>> postTrip(Map<String, dynamic> data) async =>
      await repository(() async => await _newTripDataSource.postTrip(data));
}
