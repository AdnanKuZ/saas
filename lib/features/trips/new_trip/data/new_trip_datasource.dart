import 'dart:convert';

import 'package:http/http.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/network/data_source.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/trips/models/internal_region_model.dart';
import 'package:saas/features/trips/models/region_model.dart';

class NewTripDataSource {
  final Client _client;
  NewTripDataSource(this._client);
  Future<List<RegionModel>> getRegions() async {
    return dataSource(
      () => _client.get(
        Uri.parse(regionsUrl),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}"
        },
      ),
      model: regionModelListFromJson,
    );
  }

  Future<List<InternalRegionModel>> getInternalRegions(
      String regionParent) async {
    print("URLLL:${internalRegionUrl(regionParent)}");
    return dataSource(
      () => _client.get(
        Uri.parse(internalRegionUrl(regionParent)),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}" 
        },
      ),
      model: internalRegionModelListFromJson,
    );
  }

  Future<List<RegionModel>> getExternalRegions() async {
    return dataSource(
      () => _client.get(
        Uri.parse(externalRegionUrl),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}"
        },
      ),
      model: regionModelListFromJson,
    );
  }

  Future<void> postTrip(Map<String, dynamic> data) async {
    return dataSource(
      () => _client.post(
        Uri.parse(postTripUrl),
        body: json.encode(data),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}"
        },
      ),
    );
  }

}
