import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:saas/app/extensions/date_time_helper.dart';

class NewTripPageService {
  BehaviorSubject<bool> controller = BehaviorSubject<bool>();

  int type = 1;
  String date = DateTime.now().formattedDate2;
  String time = "${TimeOfDay.now().hour}:${TimeOfDay.now().minute}:00";
  String? from;
  String? to;

  get stream => controller.stream;

  void dispose() {
    controller.close();
  }

  void setBool() => controller.sink.add(true);

  set setType(int value) => type = value + 1;
  set setDate(String value) => date = value;
  set setTime(TimeOfDay value) => time = "${value.hour}:${value.minute}:00";
  set setFrom(String value) => from = value;
  set setTo(String value) => to = value;

  void clearValues() {
    from = to = null;
  }

  Map<String, dynamic>? getData() {
    if (!checkIfNull()) {
      return type == 3
          ? {
              "type": type,
              "date": date,
              "from": from,
              "to": null,
              "time_in": time,
              "vehicles": null,
              "other_to": to,
              "note": null,
            }
          : {
              "type": type,
              "date": date,
              "from": from,
              "to": to,
              "time_in": time,
              "vehicles": null,
              "other_to": null,
              "note": null,
            };
    }
    return null;
  }

  bool checkIfNull() {
    print(
        "isChecked${(from == null || to == null)} date=$date time=$time from=$from to=$to");
    return from == null || to == null;
  }
}
