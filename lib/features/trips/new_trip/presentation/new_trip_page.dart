import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/extensions/date_time_helper.dart';
import 'package:saas/app/widgets/return_appbar.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/features/trips/models/region_model.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/post_trip_cubit.dart'
    as po;
import 'package:saas/features/trips/new_trip/presentation/cubit/regions_cubit.dart';
import 'package:saas/features/trips/new_trip/presentation/trip_successful_page.dart';
import 'package:saas/features/trips/new_trip/presentation/widgets/date_select_widget.dart';
import 'package:saas/features/trips/new_trip/presentation/widgets/pickup_dropoff_widget.dart';
import 'package:saas/features/trips/new_trip/presentation/widgets/trip_type_widget.dart';
import 'package:saas/app/widgets/drop_down.dart';

const Map<int, List<String>> dataMap = {
  0: ["assets/icons/internal_trip.png", "Internal"],
  1: ["assets/icons/external_trip.png", "External"],
  2: ["assets/icons/other_trip.png", "Other"],
};

class NewTripPage extends StatefulWidget {
  const NewTripPage({super.key});

  @override
  State<NewTripPage> createState() => _NewTripPageState();
}

class _NewTripPageState extends State<NewTripPage> {
  int selectedType = 0; // 0 : internal, 1 : external, 2 : other
  late final RegionsCubit regionsCubit;
  RegionModel? selectedRegion;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    regionsCubit = BlocProvider.of<RegionsCubit>(context);
    regionsCubit.getRegions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 18),
          const CustomAppBarWithReturn(title: "New Trip"),
          const SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Please Select One",
                  style: TextStyles.selectTripTypeTextStyle,
                ),
                const SizedBox(height: 16),
                _TripTypeSelectSection(
                    selectedType: selectedType, select: selectType),
                const SizedBox(height: 26),
                const Text(
                  "Trip Details",
                  style: TextStyle(color: AppColors.mainblue, fontSize: 16),
                ),
                const SizedBox(height: 26),
                Text(
                  "Select Region:",
                  style: TextStyles.resendCodeTextStyle,
                ),
                const SizedBox(height: 8),
                BlocConsumer<RegionsCubit, RegionsState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is LoadingState) {
                        return const Center(child: LinearProgressIndicator());
                      } else if (state is GotRegionsState) {
                        List<RegionModel> regionsList = state.regions;

                        List<DropdownMenuItem<RegionModel>> items =
                            List<DropdownMenuItem<RegionModel>>.from(regionsList
                                .map((e) => DropdownMenuItem<RegionModel>(
                                    value: e, child: Text(e.name))));

                        return CustomDropDownWidget<RegionModel?>(
                          value: null,
                          borderRadius: 8,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                              borderSide: BorderSide.none),
                          fillColor: Colors.white,
                          hintWidget: const Text("Region"),
                          dropDownList: items,
                          onChanged: (value) {
                            DI.newTripPageService.setBool();
                            setState(() {
                              selectedRegion = value;
                            });
                          },
                        );
                      }
                      return CustomDropDownWidget<RegionModel?>(
                        value: null,
                        borderRadius: 8,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8),
                            borderSide: BorderSide.none),
                        fillColor: Colors.white,
                        hintWidget: const Text("Region"),
                        dropDownList: null,
                        onChanged: (value) {},
                      );
                    }),
                const SizedBox(height: 16),
                NewTripPickUpDropOffWidget(
                    region: selectedRegion, tripType: selectedType),
                const SizedBox(height: 16),
                const _DateSelectSection(),
                const SizedBox(height: 26),
                Row(
                  children: [
                    BlocConsumer<po.PostTripCubit, po.PostTripState>(
                        listener: (context, state) {
                      if (state is po.ErrorState) {
                        Fluttertoast.showToast(msg: state.message);
                      }
                      if (state is po.PostedTripState) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    const TripSuccessfulPage()));
                      }
                    }, builder: (context, state) {
                      if (state is po.LoadingState) {
                        return SizedBox(
                            width: MediaQuery.sizeOf(context).width * 0.8,
                            child: const Center(
                                child: CircularProgressIndicator()));
                      }
                      return Expanded(
                        child: CustomElevatedButton(
                          title: "Submit",
                          buttonColor: AppColors.mainblue,
                          borderRadius: 8,
                          verticalPadding: 15,
                          onPressed: () {
                            Map<String, dynamic>? data =
                                DI.newTripPageService.getData();
                            print("DataIS:${json.encode(data)}");
                            if (data == null) {
                              Fluttertoast.showToast(
                                  msg:
                                      "You haven't entered all the information");
                            } else {
                              BlocProvider.of<po.PostTripCubit>(context)
                                  .postTrip(data);
                            }
                          },
                        ),
                      );
                    }),
                  ],
                ),
                const SizedBox(height: 26),
              ],
            ),
          )
        ],
      ))),
    );
  }

  void selectType(int index) {
    print("changingType:$index");
    DI.newTripPageService.setBool();
    DI.newTripPageService.setType = index;
    setState(() {
      selectedType = index;
    });
  }
}

class _TripTypeSelectSection extends StatelessWidget {
  const _TripTypeSelectSection(
      {required this.selectedType, required this.select});
  final int selectedType;
  final Function(int index) select;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                select(0);
              },
              child: TripTypeWidget(0, dataMap[0]!, selectedType)),
        ), // Internal
        Expanded(
          child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                select(1);
              },
              child: TripTypeWidget(1, dataMap[1]!, selectedType)),
        ), // External
        Expanded(
          child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                select(2);
              },
              child: TripTypeWidget(2, dataMap[2]!, selectedType)),
        ), // Other
      ],
    );
  }
}

class _DateSelectSection extends StatefulWidget {
  const _DateSelectSection();

  @override
  State<_DateSelectSection> createState() => _DateSelectSectionState();
}

class _DateSelectSectionState extends State<_DateSelectSection> {
  DateTime currentDate = DateTime.now();
  TimeOfDay currentTime = TimeOfDay.now();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 70,
      child: Row(
        children: [
          Expanded(
              child: InkWell(
            onTap: () async {
              final DateTime? pickedDate = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime.now(),
                  lastDate: DateTime(2050));
              if (pickedDate != null && pickedDate != currentDate) {
                DI.newTripPageService.setDate = pickedDate.formattedDate2;
                setState(() {
                  currentDate = pickedDate;
                });
              }
            },
            child: DateSelectWidget(
              isDate: true,
              value: currentDate.formattedDate3,
            ),
          )),
          const SizedBox(width: 8),
          Expanded(
              child: InkWell(
            onTap: () async {
              final TimeOfDay? pickedTime = await showTimePicker(
                  context: context, initialTime: TimeOfDay.now());
              if (pickedTime != null && pickedTime != currentTime) {
                DI.newTripPageService.setTime = pickedTime;
                setState(() {
                  currentTime = pickedTime;
                });
              }
            },
            child: DateSelectWidget(
              isDate: false,
              value: DateTime(currentDate.year, currentDate.month,
                      currentDate.day, currentTime.hour, currentTime.minute)
                  .formattedTime,
            ),
          )),
        ],
      ),
    );
  }
}
