import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/trips/models/internal_region_model.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_repositories.dart';

part 'internal_regions_state.dart';

class InternalRegionsCubit extends Cubit<InternalRegionsState> {
  final NewTripRepositories _newTripRepositories;
  InternalRegionsCubit(this._newTripRepositories)
      : super(InternalRegionsInitial());
  Future<void> getInternalRegions(String regionParent) async {
    emit(LoadingState());
    final either = await _newTripRepositories.getInternalRegions(regionParent);
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotInternalRegionsState(data));
    });
  }
}
