import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/trips/models/region_model.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_repositories.dart';

part 'regions_state.dart';

class RegionsCubit extends Cubit<RegionsState> {
  final NewTripRepositories _newTripRepositories;
  RegionsCubit(this._newTripRepositories) : super(RegionsInitial());
  Future<void> getRegions() async {
    emit(LoadingState());
    final either = await _newTripRepositories.getRegions();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotRegionsState(data));
    });
  }
}
