part of 'regions_cubit.dart';

abstract class RegionsState {
  const RegionsState();
}

class RegionsInitial extends RegionsState {}

class LoadingState extends RegionsState {}

class GotRegionsState extends RegionsState {
  final List<RegionModel> regions;
  GotRegionsState(this.regions);

  List<String> getDropDownValues(List<RegionModel> regions) {
    return regions.map((region) => region.name).toList();
  }
}

class ErrorState extends RegionsState {
  final String message;

  ErrorState(this.message);
}
