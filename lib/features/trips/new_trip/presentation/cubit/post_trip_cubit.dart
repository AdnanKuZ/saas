import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_repositories.dart';

part 'post_trip_state.dart';

class PostTripCubit extends Cubit<PostTripState> {
  final NewTripRepositories _newTripRepositories;
  PostTripCubit(this._newTripRepositories) : super(PostTripInitial());
  Future<void> postTrip(Map<String, dynamic> data) async {
    emit(LoadingState());
    final either = await _newTripRepositories.postTrip(data);
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(PostedTripState());
    });
  }
}
