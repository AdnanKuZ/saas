part of 'post_trip_cubit.dart';

abstract class PostTripState {
  const PostTripState();
}

class PostTripInitial extends PostTripState {}

class LoadingState extends PostTripState {}

class PostedTripState extends PostTripState {
  PostedTripState();
}

class ErrorState extends PostTripState {
  final String message;

  ErrorState(this.message);
}
