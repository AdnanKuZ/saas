part of 'internal_regions_cubit.dart';



abstract class InternalRegionsState {
  const InternalRegionsState();
}

class InternalRegionsInitial extends InternalRegionsState {}

class LoadingState extends InternalRegionsState {}

class GotInternalRegionsState extends InternalRegionsState {
  final List<InternalRegionModel> regions;

  GotInternalRegionsState(this.regions);
}
class ErrorState extends InternalRegionsState {
  final String message;

  ErrorState(this.message);
}
