part of 'external_regions_cubit.dart';

abstract class ExternalRegionsState {
  const ExternalRegionsState();
}

class ExternalRegionsInitial extends ExternalRegionsState {}

class LoadingState extends ExternalRegionsState {}

class GotExternalRegionsState extends ExternalRegionsState {
  final List<RegionModel> regions;

  GotExternalRegionsState(this.regions);
  
}

class ErrorState extends ExternalRegionsState {
  final String message;

  ErrorState(this.message);
}
