import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/trips/models/region_model.dart';
import 'package:saas/features/trips/new_trip/data/new_trip_repositories.dart';

part 'external_regions_state.dart';

class ExternalRegionsCubit extends Cubit<ExternalRegionsState> {
  final NewTripRepositories _newTripRepositories;
  ExternalRegionsCubit(this._newTripRepositories)
      : super(ExternalRegionsInitial());
  Future<void> getExternalRegions() async {
    emit(LoadingState());
    final either = await _newTripRepositories.getExternalRegions();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotExternalRegionsState(data));
    });
  }
}
