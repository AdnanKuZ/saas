// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:saas/app/di.dart';
// import 'package:saas/app/widgets/return_appbar.dart';

// import 'package:saas/app/constants/colors.dart';
// import 'package:saas/app/constants/text_styles.dart';
// import 'package:saas/app/widgets/button.dart';
// import 'package:saas/features/trips/trip_detail/presentation/trip_detail_page.dart';

// class TripSuccessfulPage extends StatefulWidget {
//   const TripSuccessfulPage({super.key});

//   @override
//   State<TripSuccessfulPage> createState() => _TripSuccessfulPageState();
// }

// class _TripSuccessfulPageState extends State<TripSuccessfulPage> {
//   double _containerSize = 1.5;
//   double _containerOpacity = 0.0;
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     Timer(const Duration(milliseconds: 200), () {
//       setState(() {
//         _containerSize = 2;
//         _containerOpacity = 1;
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     double width = MediaQuery.sizeOf(context).width;

//     return Scaffold(
//       body: SafeArea(
//           child: SingleChildScrollView(
//               child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           const SizedBox(height: 18),
//           const Padding(
//             padding: EdgeInsets.fromLTRB(12, 0, 32, 0),
//             child: CustomAppBarWithReturn(title: "New Trip"),
//           ),
//           const SizedBox(height: 70),
//           Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 22),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 Text(
//                   "Succesful",
//                   style: TextStyles.resendCodeTextStyle,
//                 ),
//                 const SizedBox(height: 50),
//                 Center(
//                   child: AnimatedOpacity(
//                     duration: const Duration(milliseconds: 2000),
//                     curve: Curves.fastLinearToSlowEaseIn,
//                     opacity: _containerOpacity,
//                     child: AnimatedContainer(
//                         duration: const Duration(milliseconds: 2000),
//                         curve: Curves.fastLinearToSlowEaseIn,
//                         height: width / _containerSize,
//                         width: width / _containerSize,
//                         alignment: Alignment.center,
//                         child: Image.asset('assets/images/success.png')),
//                   ),
//                 ),
//                 const SizedBox(height: 70),
//                 Text(
//                   "Trip Created Successfully",
//                   style: TextStyles.resendCodeTextStyle,
//                 ),
//                 const SizedBox(height: 50),
//                 const Text(
//                   "Your request has been sent successfully, and the driver will reach you as soon as possible",
//                   textAlign: TextAlign.center,
//                   style: TextStyle(color: AppColors.mainblue, fontSize: 16),
//                 ),
//                 const SizedBox(height: 50),
//                 Row(
//                   children: [
//                     Expanded(
//                       child: CustomElevatedButton(
//                         title: "Done",
//                         buttonColor: AppColors.mainblue,
//                         borderRadius: 8,
//                         verticalPadding: 15,
//                         onPressed: () {
//                           navigatorKey.currentState.pop(context);
//                           navigatorKey.currentState.pop(context);
//                           DI.mainService.controller.jumpToTab(1);

//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           )
//         ],
//       ))),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/widgets/return_appbar.dart';

import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/features/notifications/presentation/cubit/notifications_cubit.dart';
import 'package:saas/features/trips/my_trips/presentation/cubit/my_trips_cubit.dart';

class TripSuccessfulPage extends StatelessWidget {
  const TripSuccessfulPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 18),
          Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 32, 0),
            child: CustomAppBarWithReturn(
              title: "New Trip",
            ),
          ),
          const SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Succesful",
                  style: TextStyles.resendCodeTextStyle,
                ),
                Lottie.asset('assets/success.json', height: 300),
                Text(
                  "Trip Created Successfully",
                  style: TextStyles.resendCodeTextStyle,
                ),
                const SizedBox(height: 30),
                const Text(
                  "Your request has been sent successfully, and the driver will reach you as soon as possible",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: AppColors.mainblue, fontSize: 16),
                ),
                const SizedBox(height: 80),
                Row(
                  children: [
                    Expanded(
                      child: CustomElevatedButton(
                        title: "Done",
                        buttonColor: AppColors.mainblue,
                        borderRadius: 8,
                        verticalPadding: 15,
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          BlocProvider.of<NotificationsCubit>(context)
                              .getNotifications();
                          DI.mainService.controller.jumpToTab(1);
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ))),
    );
  }
}
