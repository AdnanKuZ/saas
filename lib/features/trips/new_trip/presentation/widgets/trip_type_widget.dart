import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';

class TripTypeWidget extends StatelessWidget {
  const TripTypeWidget(this.index, this.data, this.selectedType, {super.key});
  final int index;
  final int selectedType;
  final List<String> data;
  @override
  Widget build(BuildContext context) {
    bool isSelected = index == selectedType;
    return AnimatedContainer(
      duration: const Duration(milliseconds: 100),
      padding: const EdgeInsets.all(12),
      margin: const EdgeInsets.symmetric(horizontal: 4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color:isSelected? Colors.grey.shade300:Colors.grey.shade200)),
      child: Center(
          child: Column(
        children: [
          Container(
            width: 45,
            padding: const EdgeInsets.all(6),
            decoration: BoxDecoration(
              color: isSelected ? Color(0xFFF01738) : AppColors.mainblue,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Center(child: Image.asset(data[0])),
          ),
          const SizedBox(height: 20),
          Text(
            data[1],
            style:
                TextStyle(color: isSelected ? AppColors.mainblue : Color(0xFFF01738)),
          )
        ],
      )),
    );
  }
}
