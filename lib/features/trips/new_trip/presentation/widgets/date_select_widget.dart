import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';

class DateSelectWidget extends StatelessWidget {
  const DateSelectWidget(
      {super.key, required this.isDate, required this.value});
  final bool isDate;
  final String value;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(12)),
      child: Center(
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            SizedBox(
                width: 24, child: Image.asset(isDate ? 'assets/icons/calendar.png':'assets/icons/clock.png')),
            const SizedBox(width: 16),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  isDate ? "Pick-Up Date".toUpperCase() : "Pick-Up Time".toUpperCase(),
                  style: TextStyles.tripLightTextStyle,
                ),
                const SizedBox(height: 7),
                Text(
                  value,
                  style: TextStyles.tripMediumTextStyle,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
