import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/extensions/dialog_build_context.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/app/widgets/drop_down.dart';
import 'package:saas/app/widgets/text_form_field.dart';
import 'package:saas/features/trips/models/internal_region_model.dart';
import 'package:saas/features/trips/models/region_model.dart';
import 'package:saas/features/trips/new_trip/presentation/cubit/external_regions_cubit.dart'
    as ex;
import 'package:saas/features/trips/new_trip/presentation/cubit/internal_regions_cubit.dart';

import '../../../../../app/app.dart';

const emptyLocationValue = "________________";

class NewTripPickUpDropOffWidget extends StatefulWidget {
  const NewTripPickUpDropOffWidget(
      {super.key, this.region, required this.tripType});
  final RegionModel? region;
  final int tripType; // 0 : internal, 1 : external, 2 : other
  @override
  State<NewTripPickUpDropOffWidget> createState() =>
      _NewTripPickUpDropOffWidgetState();
}

class _NewTripPickUpDropOffWidgetState
    extends State<NewTripPickUpDropOffWidget> {
  String pickUpLocation = emptyLocationValue;
  String dropOffLocation = emptyLocationValue;
  @override
  void initState() {
    super.initState();
    DI.newTripPageService.controller.listen((value) {
      DI.newTripPageService.clearValues();
      setState(() {
        pickUpLocation = emptyLocationValue;
        dropOffLocation = emptyLocationValue;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (widget.region == null) {
          Fluttertoast.showToast(msg: 'You need to select a region first');
        } else {
          context
              .showDialog(CustomDialog(
            region: widget.region!,
            tripType: widget.tripType,
          ))
              .then((dynamic data) {
            if (data != null && data.length == 2) {
              setState(() {
                pickUpLocation = data[0];
                dropOffLocation = data[1];
              });
            }
          });
        }
      },
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(12)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width: 25,
                    child: Image.asset("assets/icons/pickup_blue.png")),
                const SizedBox(width: 14),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Pick-up Location".toUpperCase(),
                        style: TextStyles.tripLightTextStyle,
                      ),
                      const SizedBox(height: 7),
                      Text(
                        pickUpLocation,
                        style: TextStyles.tripMediumTextStyle,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12, top: 14, bottom: 14),
              child: DottedLine(
                direction: Axis.vertical,
                alignment: WrapAlignment.center,
                lineLength: 50,
                lineThickness: 1.0,
                dashLength: 5.0,
                dashColor: Colors.black,
                dashGradient: [Colors.grey.shade300, Colors.grey.shade300],
                dashRadius: 0.0,
                dashGapLength: 4.0,
                dashGapColor: Colors.white,
                dashGapGradient: const [Colors.white, Colors.white],
                dashGapRadius: 0.0,
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width: 25,
                    child: Image.asset("assets/icons/pickup_red.png")),
                const SizedBox(width: 14),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Drop-off Location".toUpperCase(),
                        style: TextStyles.tripLightTextStyle,
                      ),
                      const SizedBox(height: 7),
                      Text(
                        dropOffLocation,
                        style: TextStyles.tripMediumTextStyle,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CustomDialog extends StatefulWidget {
  const CustomDialog({super.key, required this.region, required this.tripType});
  final RegionModel region;
  final int tripType; // 0 : internal, 1 : external, 2 : other
  @override
  State<CustomDialog> createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {
  String? pickUpLocation;
  String? pickUpParent;
  String? dropOffLocation;
  late final InternalRegionsCubit internalRegionsCubit;
  late final ex.ExternalRegionsCubit externalRegionsCubit;
  @override
  void initState() {
    super.initState();
    internalRegionsCubit = BlocProvider.of<InternalRegionsCubit>(context);
    externalRegionsCubit = BlocProvider.of<ex.ExternalRegionsCubit>(context);
    if (widget.tripType == 1) {
      externalRegionsCubit.getExternalRegions();
    }
    internalRegionsCubit.getInternalRegions(widget.region.parent);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: const Icon(Icons.arrow_back),
                ),
                const Text(
                  'Choose locations',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox()
              ],
            ),
            const SizedBox(height: 36),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                _internalRegionsDropDown(false),
                const SizedBox(height: 26),
                widget.tripType == 0
                    ? _internalRegionsDropDown(true)
                    : widget.tripType == 1
                        ? _externalRegionsDropDown()
                        : CustomTextFormField(
                            hintText: "Other",
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  color: AppColors.textFieldBorderColor,
                                  width: 2,
                                )),
                            enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  color: AppColors.textFieldBorderColor,
                                  width: 2,
                                )),
                            onChanged: (text) => dropOffLocation = text),
                const SizedBox(height: 36),
                Row(
                  children: [
                    Expanded(
                      child: CustomElevatedButton(
                        title: "Confirm",
                        buttonColor: AppColors.mainblue,
                        borderRadius: 8,
                        verticalPadding: 15,
                        onPressed: () {
                          if (pickUpLocation != null &&
                              dropOffLocation != null &&
                              pickUpParent != null) {
                            DI.newTripPageService.setFrom = pickUpParent!;
                            DI.newTripPageService.setTo = dropOffLocation!;
                            navigatorKey.currentState!
                                .pop([pickUpLocation, dropOffLocation]);
                          } else {
                            Fluttertoast.showToast(msg: "From/To are required");
                          }
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _internalRegionsDropDown(bool isDropOff) =>
      BlocConsumer<InternalRegionsCubit, InternalRegionsState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is LoadingState) {
              return const Center(child: LinearProgressIndicator());
            } else if (state is GotInternalRegionsState) {
              List<InternalRegionModel> regionsList = state.regions;

              List<DropdownMenuItem<InternalRegionModel>> items =
                  List<DropdownMenuItem<InternalRegionModel>>.from(regionsList
                      .map((e) => DropdownMenuItem<InternalRegionModel>(
                          value: e, child: Text(e.name))));
              if (items.isEmpty) {
                return const Text("No Locations");
              }
              return CustomDropDownWidget<InternalRegionModel?>(
                value: null,
                borderRadius: 8,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide.none),
                fillColor: Colors.white,
                hintWidget: Text(isDropOff ? "To" : "From"),
                dropDownList: items,
                onChanged: (value) {
                  if (isDropOff) {
                    dropOffLocation = value!.buildingId;
                  } else {
                    pickUpLocation = value!.name;
                    pickUpParent = value.id;
                  }
                },
              );
            }
            return CustomDropDownWidget<RegionModel?>(
              value: null,
              borderRadius: 8,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide.none),
              fillColor: Colors.white,
              hintWidget: const Text("No Internet Connection"),
              dropDownList: null,
              onChanged: (value) {},
            );
          });

  Widget _externalRegionsDropDown() =>
      BlocConsumer<ex.ExternalRegionsCubit, ex.ExternalRegionsState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is ex.LoadingState) {
              return const Center(child: LinearProgressIndicator());
            } else if (state is ex.GotExternalRegionsState) {
              List<RegionModel> regionsList = state.regions;

              List<DropdownMenuItem<RegionModel>> items =
                  List<DropdownMenuItem<RegionModel>>.from(regionsList.map(
                      (e) => DropdownMenuItem<RegionModel>(
                          value: e, child: Text(e.name))));
              if (items.isEmpty) {
                return const Text("No Locations");
              }
              return CustomDropDownWidget<RegionModel?>(
                value: null,
                borderRadius: 8,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide.none),
                fillColor: Colors.white,
                hintWidget: const Text("To"),
                dropDownList: items,
                onChanged: (value) {
                  dropOffLocation = value!.parent;
                },
              );
            }
            return CustomDropDownWidget<RegionModel?>(
              value: null,
              borderRadius: 8,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide.none),
              fillColor: Colors.white,
              hintWidget: const Text("Region"),
              dropDownList: null,
              onChanged: (value) {},
            );
          });
}
