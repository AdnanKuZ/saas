part of 'boarding_pages_cubit.dart';
abstract class BoardingPagesState {
  const BoardingPagesState();
}

class BoardingPagesInitial extends BoardingPagesState {}

class LoadingState extends BoardingPagesState {}

class GotBoardingPagesState extends BoardingPagesState {
  final StaticPagesModel model;

  GotBoardingPagesState(this.model);
  
}

class ErrorState extends BoardingPagesState {
  final String message;

  ErrorState(this.message);
}
