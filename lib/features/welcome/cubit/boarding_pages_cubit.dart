import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';
import 'package:saas/features/statics/data/static_pages_repositories.dart';

part 'boarding_pages_state.dart';

class BoardingPagesCubit extends Cubit<BoardingPagesState> {
  final StaticPagesRepositories _staticPagesRepositories;
  BoardingPagesCubit(this._staticPagesRepositories)
      : super(BoardingPagesInitial());
  Future<void> getBoardingPages() async {
    emit(LoadingState());
    final either = await _staticPagesRepositories.getWelcomePages();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotBoardingPagesState(data));
    });
  }
}
