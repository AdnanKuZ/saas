import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';

class BoardingWidget extends StatelessWidget {
  final String imagePath;
  final String title;
  final bool dynamic;
  const BoardingWidget(
      {super.key,
      required this.imagePath,
      required this.title,
      this.dynamic = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: Align(
        alignment: const Alignment(0, -0.5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Text(
                title,
                style: TextStyles.boardingWidgetTextStyle,
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 80,
            ),
            Flexible(
              child: Center(
                child: dynamic
                    ? Image.network(
                        imagePath,
                        fit: BoxFit.contain,
                      )
                    : Image.asset(
                        imagePath,
                        fit: BoxFit.contain,
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
