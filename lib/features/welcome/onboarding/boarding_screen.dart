import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/authentication/presentation/login_page.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';
import 'package:saas/features/welcome/cubit/boarding_pages_cubit.dart';
import 'package:saas/features/welcome/onboarding/widgets/boarding_widget.dart';
import 'package:saas/features/welcome/onboarding/widgets/indicator.dart';

class BoardingScreen extends StatefulWidget {
  const BoardingScreen({super.key});

  @override
  State<BoardingScreen> createState() => _BoardingScreenState();
}

class _BoardingScreenState extends State<BoardingScreen> {
  static const _titles = [
    'SAAS is a company that specializes in mobility solutions inside and outside airports for local and international companies',
    'Crafting pleasant mobility experience',
    'Pioneers in providing creative mobility solutions',
  ];
  static const _boardingImages = [
    "assets/images/onboarding1.png",
    "assets/images/onboarding2.png",
    "assets/images/onboarding3.png",
  ];
  late final PageController _controller;
  late int _currentPageIndex;
  List<Datum>? data;
  @override
  void initState() {
    super.initState();
    _currentPageIndex = 0;
    _controller = PageController(initialPage: _currentPageIndex);
  }

  int get _lastIndex => data == null ? _titles.length - 1 : data!.length - 1;

  bool get _isLastPage => _currentPageIndex == _lastIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(height: 80),
            Image.asset(
              "assets/images/logo.png",
              fit: BoxFit.contain,
            ),
            const SizedBox(height: 16),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.6,
              child: BlocConsumer<BoardingPagesCubit, BoardingPagesState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is GotBoardingPagesState) {
                      print("entered GotBoardingPagesState");
                      List<String> _dynamicTitles = [];
                      List<String> _dynamicBoardingImages = [];

                      data = state.model.data;

                      List<Datum> _data = state.model.data;
                      for (Datum i in _data) {
                        _dynamicTitles.add(i.text ?? "");
                        _dynamicBoardingImages.add("$imageUrl${i.image}");
                      }

                      return PageView.builder(
                        controller: _controller,
                        onPageChanged: (index) {
                          if (index == _lastIndex) {}
                          setState(() {
                            _currentPageIndex = index;
                          });
                        },
                        itemBuilder: (context, index) => BoardingWidget(
                            imagePath: _dynamicBoardingImages[index],
                            title: _dynamicTitles[index],
                            dynamic: true),
                        itemCount: _data.length,
                      );
                    }
                    if(state is LoadingState){
                      return const Center(child: CircularProgressIndicator(),);
                    }
                    return PageView.builder(
                      controller: _controller,
                      onPageChanged: (index) {
                        if (index == _lastIndex) {}
                        setState(() {
                          _currentPageIndex = index;
                        });
                      },
                      itemBuilder: (context, index) => BoardingWidget(
                        imagePath: _boardingImages[index],
                        title: _titles[index],
                      ),
                      itemCount: _titles.length,
                    );
                  }),
            ),
            CustomElevatedButton(
              title: _isLastPage ? "Start Exploring" : "Next",
              buttonColor: AppColors.mainblue,
              borderRadius: 8,
              horizantalPadding: _isLastPage ? 70 : 115,
              verticalPadding: 15,
              onPressed: () {
                if (_isLastPage) {
                  DI.welcomeService.setIsFirstTime();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()));
                } else {
                  setState(() {
                    _nextPage();
                  });
                }
              },
            ),
            const SizedBox(height: 32),
            Center(
              child: Indicator(controller: _controller, length: _titles.length),
            ),
          ],
        ),
      ),
    );
  }

  void _nextPage() => _toPage(_currentPageIndex + 1);
  void _skip() {
    DI.welcomeService.setIsFirstTime();
    _toPage(_lastIndex);
  }

  void _toPage(int page) {
    if (page < 0 || page > _lastIndex) return;

    _controller.animateToPage(page,
        duration: const Duration(milliseconds: 250), curve: Curves.easeInOut);
  }
}
