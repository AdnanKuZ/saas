import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/link_text.dart';
import 'package:saas/features/authentication/presentation/login_page.dart';
import 'package:saas/features/welcome/cubit/boarding_pages_cubit.dart';
import 'package:saas/features/welcome/onboarding/boarding_screen.dart';
import 'package:saas/app/di.dart';


class WelcomePage extends StatelessWidget {
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const Expanded(flex: 1, child: SizedBox()),
          Expanded(
              flex: 2,
              child: SizedBox(
                width: MediaQuery.sizeOf(context).width,
                child: Image.asset("assets/images/welcome_screen_image.png"),
              )),
          const Expanded(flex: 1, child: SizedBox()),
          Expanded(
              flex: 2,
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.only(
                    right: 28, left: 28, top: 38, bottom: 28),
                decoration: const BoxDecoration(
                    color: AppColors.mainblue,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(17),
                        topRight: Radius.circular(17))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Welcome",
                          style: TextStyles.welcomePageTextStyle1,
                        ),
                        const SizedBox(height: 18),
                        Text("Saudi Amad For Airport",
                            style: TextStyles.welcomePageTextStyle2),
                        const SizedBox(height: 8),
                        Text("Service & Transport Support",
                            style: TextStyles.welcomePageTextStyle2),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        LinkText("Skip",
                            color: Colors.white, isUnderLine: false, onTap: () {
                          DI.welcomeService.setIsFirstTime();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()));
                        }),
                        LinkText("> Trip",
                            color: Colors.white, isUnderLine: false, onTap: () {
                          BlocProvider.of<BoardingPagesCubit>(context)
                              .getBoardingPages();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const BoardingScreen()));
                        }),
                      ],
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
