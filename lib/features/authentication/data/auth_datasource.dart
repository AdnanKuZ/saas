import 'dart:convert';

import 'package:http/http.dart';
import 'package:saas/core/network/data_source.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/authentication/data/user_model.dart';
class AuthDataSource {
  final Client _client;
  AuthDataSource(this._client);
  Future<UserModel> login(Map<String, dynamic> data) async {
    return dataSource(
      () => _client.post(
        Uri.parse(loginUrl),
        body: json.encode(data),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      ),
      model: userModelFromJson,
      isAuth: true
    );
  }
}
