import 'dart:convert';

import 'package:saas/app/di.dart';

UserModel userModelFromJson(String str, bool isFromLocalStorage) {
  if(isFromLocalStorage){return UserModel.fromJson(json.decode(str));}
  final jsonData = json.decode(str)["data"];
  dynamic finalData = {};
  print(jsonData.length);
  if (jsonData.length>2) {
    finalData = {
      "token": DI.userService.getUser()?.token ?? "",
      "user": jsonData
    };
  } else {
    finalData = jsonData;
  }
  return UserModel.fromJson(finalData);
}

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  String token;
  User user;

  UserModel({
    required this.token,
    required this.user,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        token: json["token"],
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "token": token,
        "user": user.toJson(),
      };
}

class User {
  int id;
  String name;
  String email;
  int type;
  int? status;
  int? userId;
  dynamic stationId;
  String? mobile;
  String? gender;
  dynamic mypassword;
  String? userType;
  dynamic groupId;
  dynamic prn;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;
  List<dynamic>? metas;
  List<dynamic>? metaData;

  User({
    required this.id,
    required this.type,
    this.status,
    this.userId,
    this.stationId,
    required this.name,
    required this.email,
    this.mobile,
    this.gender,
    this.mypassword,
    this.userType,
    this.groupId,
    this.prn,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.metas,
    this.metaData,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
      id: json["id"],
      type: json["type"],
      status: json["status"],
      userId: json["user_id"],
      stationId: json["station_id"],
      name: json["name"],
      email: json["email"],
      mobile: json["mobile"],
      gender: json["gender"],
      mypassword: json["mypassword"],
      userType: json["user_type"],
      groupId: json["group_id"],
      prn: json["prn"],
      createdAt: json["created_at"] != null
          ? DateTime.parse(json["created_at"])
          : DateTime.now(),
      updatedAt: json["updated_at"] != null
          ? DateTime.parse(json["updated_at"])
          : DateTime.now(),
      deletedAt: json["deleted_at"],
      metas: json["metas"] != null
          ? List<dynamic>.from(json["metas"].map((x) => x))
          : null,
      metaData: json["meta_data"] != null
          ? List<dynamic>.from(json["meta_data"].map((x) => x))
          : null);
  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "status": status,
        "user_id": userId,
        "station_id": stationId,
        "name": name,
        "email": email,
        "mobile": mobile,
        "gender": gender,
        "mypassword": mypassword,
        "user_type": userType,
        "group_id": groupId,
        "prn": prn,
        "created_at": createdAt?.toIso8601String() ?? "",
        "updated_at": updatedAt?.toIso8601String() ?? "",
        "deleted_at": deletedAt,
        "metas": metas != null ? List<dynamic>.from(metas!.map((x) => x)) : [],
        "meta_data":
            metaData != null ? List<dynamic>.from(metaData!.map((x) => x)) : [],
      };
}
