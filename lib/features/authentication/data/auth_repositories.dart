import 'package:dartz/dartz.dart';
import 'package:saas/core/error/failures.dart';
import 'package:saas/core/network/repositories.dart';
import 'package:saas/features/authentication/data/auth_datasource.dart';
import 'package:saas/features/authentication/data/user_model.dart';

class AuthRepositories {
  final AuthDataSource _authDataSource;
  AuthRepositories(this._authDataSource);

  Future<Either<Failure, UserModel>> login(Map<String, dynamic> data) async =>
      await repository(() async => await _authDataSource.login(data));
}
