import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/text_form_field.dart';
import 'package:saas/core/funcs.dart';

class EmailTextField extends StatelessWidget {
  const EmailTextField(this.controller, {super.key});
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return CustomTextFormField(
      controller: controller,
      textStyle: TextStyles.textFieldTextStyle,
      hintText: "Email",
      border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: AppColors.textFieldBorderColor,
            width: 2,
          )),
      enabledBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        borderSide: BorderSide(
          color: AppColors.textFieldBorderColor,
          width: 2,
        ),
      ),
      disabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide.none),
      errorBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: Color(0xFFF01738),
          )),
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 15),
      fillColor: Colors.white,
      suffix: InkWell(
          onTap: () => controller.clear(), child: const Icon(Icons.clear)),
      validator: emailValidator,
    );
  }
}
