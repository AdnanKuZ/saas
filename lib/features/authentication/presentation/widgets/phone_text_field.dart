// import 'package:flutter/material.dart';
// import 'package:intl_phone_field/intl_phone_field.dart';
// import 'package:saas/app/constants/colors.dart';
// import 'package:saas/app/constants/text_styles.dart';

// class PhoneTextField extends StatelessWidget {
//   const PhoneTextField(this.controller, {super.key});
//   final TextEditingController controller;
//   @override
//   Widget build(BuildContext context) {
//     return IntlPhoneField(
//       controller: controller,
//       countries: const ["SA"],
//       showCountryFlag: false,
//       showDropdownIcon: false,
//       style: TextStyles.textFieldTextStyle,
//       dropdownTextStyle: TextStyles.textFieldTextStyle,
//       decoration: InputDecoration(
//         labelText: "Phone Number",
//         border: OutlineInputBorder(
//             borderRadius: const BorderRadius.all(Radius.circular(10)),
//             borderSide: BorderSide(
//               color: AppColors.textFieldBorderColor,
//               width: 2,
//             )),
//         enabledBorder: OutlineInputBorder(
//           borderRadius: const BorderRadius.all(Radius.circular(10)),
//           borderSide: BorderSide(
//             color: AppColors.textFieldBorderColor,
//             width: 2,
//           ),
//         ),
//         disabledBorder: const OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(10)),
//             borderSide: BorderSide.none),
//         errorBorder: const OutlineInputBorder(
//             borderRadius: BorderRadius.all(Radius.circular(10)),
//             borderSide: BorderSide(
//               color: Color(0xFFF01738),
//             )),
//         contentPadding:
//             const EdgeInsets.symmetric(horizontal: 14, vertical: 15),
//         filled: true,
//         fillColor: Colors.white,
//         suffixIcon: InkWell(
//             onTap: () => controller.clear(), child: const Icon(Icons.clear)),
//       ),
//       initialCountryCode: 'SA',
//       flagsButtonPadding: const EdgeInsets.only(left: 16, right: 10),
//       onChanged: (phone) {
//         print(phone.completeNumber);
//       },
//     );
//   }
// }
