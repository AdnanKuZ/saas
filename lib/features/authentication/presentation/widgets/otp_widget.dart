// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';

// import '../../../../app/constants/colors.dart';

// class OtpWidget extends StatelessWidget {
//   const OtpWidget({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return OtpTextField(
//       numberOfFields: 4,
//       borderColor: AppColors.textFieldBorderColor,
//       focusedBorderColor: AppColors.mainblue,
//       cursorColor: AppColors.mainblue,
//       borderWidth: 3,
//       fieldWidth: 45,
//       inputFormatters: [FilteringTextInputFormatter.digitsOnly],
//       showFieldAsBox: false,
//       //runs when a code is typed in
//       onCodeChanged: (String code) {},
//       //runs when every textfield is filled
//       onSubmit: (String verificationCode) {
//         showDialog(
//             context: context,
//             builder: (context) {
//               return AlertDialog(
//                 title: const Text("Verification Code"),
//                 content: Text('Code entered is $verificationCode'),
//               );
//             });
//       },
//     );
//   }
// }
