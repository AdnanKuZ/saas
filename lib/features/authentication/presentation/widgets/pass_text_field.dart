import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/text_form_field.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/authentication/presentation/widgets/show_pass_widget.dart';

class PassTextField extends StatefulWidget {
  const PassTextField(this.controller, {super.key});
  final TextEditingController controller;

  @override
  State<PassTextField> createState() => _PassTextFieldState();
}

class _PassTextFieldState extends State<PassTextField> {
  bool isHidden = false;

  @override
  Widget build(BuildContext context) {
    return CustomTextFormField(
      controller: widget.controller,
      textStyle: TextStyles.textFieldTextStyle,
      hintText: "Password",
      border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: AppColors.textFieldBorderColor,
            width: 2,
          )),
      enabledBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        borderSide: BorderSide(
          color: AppColors.textFieldBorderColor,
          width: 2,
        ),
      ),
      disabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide.none),
      errorBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: Color(0xFFF01738),
          )),
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 15),
      fillColor: Colors.white,
      isObscur: isHidden,
      suffix: ShowPassWidget(
        showHide: () {
          setState(() {
            isHidden = !isHidden;
          });
        },
        isHidden: isHidden,
      ),
      validator: passwordValidator,
    );
  }
}
