import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/button.dart';
import 'package:saas/features/authentication/presentation/cubit/auth_cubit.dart';
import 'package:saas/features/authentication/presentation/widgets/email_text_field.dart';
import 'package:saas/features/authentication/presentation/widgets/pass_text_field.dart';
import 'package:saas/main_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController emailCon = TextEditingController();
  final TextEditingController passCon = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: SingleChildScrollView(
        child: Container(
          width: MediaQuery.sizeOf(context).width,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 18),
                Text(
                  "Login",
                  style: TextStyles.pageTitleTextStyle,
                ),
                SizedBox(height: MediaQuery.sizeOf(context).height * 0.1),
                Text(
                  "Welcome back!",
                  style: TextStyles.secondaryPageTitleTextStyle,
                ),
                const SizedBox(height: 14),
                Text(
                  "Log in to your existant account of Discoa.",
                  style: TextStyles.smallLightTextStyle,
                ),
                const SizedBox(height: 30),
                Text(
                  "What’s your mobile \nnumber?",
                  textAlign: TextAlign.center,
                  style: TextStyles.boardingWidgetTextStyle,
                ),
                const SizedBox(height: 40),
                EmailTextField(emailCon),
                const SizedBox(height: 16),
                PassTextField(passCon),
                const SizedBox(height: 50),
                Text(
                  "By continuing, I confirm that i have read & agree to the",
                  textAlign: TextAlign.center,
                  style: TextStyles.policyTextStyle,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {},
                      child: const Text("Terms & conditions",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: "NunitoSans",
                            fontSize: 12,
                          )),
                    ),
                    Text(
                      "and ",
                      textAlign: TextAlign.center,
                      style: TextStyles.policyTextStyle,
                    ),
                    InkWell(
                      onTap: () {},
                      child: const Text("Privacy policy",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: "NunitoSans",
                            fontSize: 12,
                          )),
                    ),
                  ],
                ),
                const SizedBox(height: 36),
                BlocConsumer<AuthCubit, AuthState>(listener: (context, state) {
                  if (state is AuthenticatedState) {
                    // context.showSuccessSnackBar('Login Successful');
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const MainPage()));
                  } else if (state is ErrorState) {
                    Fluttertoast.showToast(msg: state.message);
                  }
                }, builder: (context, state) {
                  if (state is LoadingState) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  return Row(
                    children: [
                      Expanded(
                        child: CustomElevatedButton(
                          title: "Continue",
                          buttonColor: AppColors.mainblue,
                          borderRadius: 8,
                          verticalPadding: 15,
                          onPressed: () {
                            Map<String, dynamic> data = {
                              "email": emailCon.text,
                              "password": passCon.text
                            };
                            if (_formKey.currentState!.validate()) {
                              BlocProvider.of<AuthCubit>(context).login(data);
                            }
                          },
                        ),
                      ),
                    ],
                  );
                }),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
