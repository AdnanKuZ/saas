// import 'package:flutter/material.dart';
// import 'package:saas/app/constants/colors.dart';
// import 'package:saas/app/constants/text_styles.dart';
// import 'package:saas/app/page_transition.dart';
// import 'package:saas/app/widgets/return_appbar.dart';
// import 'package:saas/app/widgets/button.dart';
// import 'package:saas/features/authentication/presentation/widgets/otp_widget.dart';
// import 'package:saas/main_page.dart';


// class OtpPage extends StatelessWidget {
//   const OtpPage({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: SafeArea(
//           child: SingleChildScrollView(
//         child: Container(
//           width: MediaQuery.sizeOf(context).width,
//           padding: const EdgeInsets.symmetric(horizontal: 20),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               const SizedBox(height: 18),
//               const CustomAppBarWithReturn(),
//               SizedBox(height: MediaQuery.sizeOf(context).height * 0.2),
//               Text(
//                 "Verification",
//                 style: TextStyles.secondaryPageTitleTextStyle,
//               ),
//               const SizedBox(height: 18),
//               Text(
//                 "Enter the 4-digit code sent to you at",
//                 style: TextStyles.smallLightTextStyle,
//               ),
//               const SizedBox(height: 50),
//               Text(
//                 "verification code we sent To +966504984956.",
//                 textAlign: TextAlign.center,
//                 style: TextStyles.boardingWidgetTextStyle,
//               ),
//               const SizedBox(height: 60),
//               const OtpWidget(),
//               const SizedBox(height: 50),
//               Row(
//                 children: [
//                   Expanded(
//                     child: CustomElevatedButton(
//                       title: "Submit",
//                       buttonColor: AppColors.mainblue,
//                       borderRadius: 8,
//                       verticalPadding: 15,
//                       onPressed: () {
//                         Navigator.pushAndRemoveUntil(
//                      context,
//                           PageTransition(widget: const MainPage()),
//                           (Route<dynamic> route) => false,
//                         );
//                       },
//                     ),
//                   ),
//                 ],
//               ),
//               const SizedBox(height: 18),
//               Text(
//                 "Resend code in 02:59",
//                 style: TextStyles.resendCodeTextStyle,
//               ),
//             ],
//           ),
//         ),
//       )),
//     );
//   }
// }
