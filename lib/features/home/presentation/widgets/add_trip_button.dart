import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent-tab-view.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/features/trips/new_trip/presentation/new_trip_page.dart';

class AddTripButton extends StatelessWidget {
  const AddTripButton({super.key, this.height, this.width});
  final double? width;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          // pushNewScreen(context, screen:   TripDetailPage(trip: Trip(id: 123,tripId: 123,date: DateTime.now(),timeIn: DateTime.now().formattedTime,from: "Damascus",to: "Hammah",fromBuilding: "asdas",fromGroup: "asdasd",fromRegion: "asdsad"),),withNavBar: false);
          pushNewScreen(context, screen:   const NewTripPage(),withNavBar: false);
        },
        child: Container(
          width: width ?? 55,
          height: height ?? 65,
          padding: const EdgeInsets.only(top: 3),
          decoration: const BoxDecoration(
              color: AppColors.mainblue, shape: BoxShape.circle),
          child: const Icon(
            CupertinoIcons.add,
            color: Colors.white,
            size: 36,
          ),
        ));
  }
}
