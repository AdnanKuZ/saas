import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';

class NewsWidget extends StatelessWidget {
  const NewsWidget({super.key, this.data});
  final Datum? data;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            color: Colors.grey,
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(data?.title??"SaaS takes the “icon of mob",
                  style: TextStyles.newsWidgetTextStyle),
              const SizedBox(height: 7),
              Text(data?.text??"The Saudi and Arab media highlighted\nthe SaaS company’s",
                  style: TextStyles.smallLightTextStyle),
            ],
          ),
        )
      ],
    );
  }
}
