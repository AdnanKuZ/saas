import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';

class CarouselSection extends StatefulWidget {
  const CarouselSection({super.key, this.data});
  final StaticPagesModel? data;
  @override
  State<CarouselSection> createState() => _CarouselSectionState();
}

class _CarouselSectionState extends State<CarouselSection> {
  int _current = 0;
  final CarouselController _controller = CarouselController();
  late List<String> imgList;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    imgList = widget.data == null
        ? [
            "assets/images/slider_image.png",
            "assets/images/slider_image.png",
            "assets/images/slider_image.png"
          ]
        : widget.data!.getListOfDatumImages();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: CarouselSlider(
            carouselController: _controller,
            items: imgList
                .map((e) =>
                    widget.data == null ? Image.asset(e) : Image.network("$imageUrl$e",loadingBuilder: (context, child, loadingProgress) => Image.asset("assets/images/slider_image.png"),))
                .toList(),
            options: CarouselOptions(
              height: 200,
              aspectRatio: 16 / 9,
              viewportFraction: 1,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 3),
              autoPlayAnimationDuration: const Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              enlargeFactor: 0.3,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: imgList.asMap().entries.map((entry) {
            bool _ = _current == entry.key;
            return AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              width: _ ? 22.0 : 4,
              height: 4.0,
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: (Theme.of(context).brightness == Brightness.dark
                          ? Colors.white
                          : AppColors.mainblue)
                      .withOpacity(_ ? 0.9 : 0.4)),
            );
          }).toList(),
        )
      ],
    );
  }
}
