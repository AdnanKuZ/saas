import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/di.dart';
import 'package:saas/app/widgets/shimmer.dart';
import 'package:saas/core/drawer_functions.dart';
import 'package:saas/features/home/presentation/cubit/home_news_cubit.dart'
    as homeNews;
import 'package:saas/features/home/presentation/cubit/home_slider_cubit.dart'
    as homeSlider;
import 'package:saas/features/home/presentation/widgets/add_trip_button.dart';
import 'package:saas/features/home/presentation/widgets/carousel_section.dart';
import 'package:saas/features/home/presentation/widgets/news_widget.dart';
import 'package:saas/features/profile/presentation/cubit/profile_cubit.dart';
import 'package:saas/features/statics/presentation/cubit/about_us_cubit.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final DrawerUtils drawerUtils = DI.drawer;

  int selectedTabIndex = 0;
  MenuEnum selectedMenuItem = MenuEnum.Home;

  @override
  void initState() {
    drawerUtils.MenuStatecontroller.listen((value) {
      setState(() {
        selectedMenuItem = value.selectedMenuItem;
      });
    });
    super.initState();
    _getCubits();
  }

  void _getCubits() {
    BlocProvider.of<ProfileCubit>(context).getCustomer();
    BlocProvider.of<AboutUsCubit>(context).getAboutUS();
    BlocProvider.of<homeSlider.HomeSliderCubit>(context).getSlider();
    BlocProvider.of<homeNews.HomeNewsCubit>(context).getNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: drawerUtils.RenderWidget(selectedMenuItem));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          BlocConsumer<homeSlider.HomeSliderCubit, homeSlider.HomeSliderState>(
            listener: (context, state) {},
            builder: (context, state) {
              if (state is homeSlider.LoadingState) {
                return shimmer(const HomeSliderShimmerWidget());
              }
              if (state is homeSlider.GotSlidersState) {
                return CarouselSection(data: state.model);
              }
              return const CarouselSection();
            },
          ),
          const SizedBox(height: 30),
          const _AddTripSection(),
          const SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Text(
              "Last News",
              style: TextStyles.pageBlueTextStyle,
            ),
          ),
          const SizedBox(height: 40),
          BlocConsumer<homeNews.HomeNewsCubit, homeNews.HomeNewsState>(
            listener: (context, state) {},
            builder: (context, state) {
              if (state is homeNews.LoadingState) {
                return  shimmer(const HomeNewsShimmerWidget());
              }
              if (state is homeNews.GotNewsState) {
                return ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: state.model.data.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 30),
                    itemBuilder: (context, index) {
                      return NewsWidget(data: state.model.data[index]);
                    });
              }
              return const NewsWidget();
            },
          ),const SizedBox(height: 40),
        ],
      ),
    );
  }
}

class _AddTripSection extends StatelessWidget {
  const _AddTripSection();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("New Trip", style: TextStyles.resendCodeTextStyle),
                    const SizedBox(height: 7),
                    Text("You can request a new trip through",
                        style: TextStyles.smallLightTextStyle),
                    const SizedBox(height: 4),
                    Text("the following link",
                        style: TextStyles.smallLightTextStyle),
                  ],
                ),
              ),
              const SizedBox(width: 5),
              const Expanded(flex: 1, child: AddTripButton()),
            ],
          ),
        ],
      ),
    );
  }
}
