part of 'home_slider_cubit.dart';

abstract class HomeSliderState {
  const HomeSliderState();
}

class HomeSliderInitial extends HomeSliderState {}

class LoadingState extends HomeSliderState {}

class GotSlidersState extends HomeSliderState {
  final StaticPagesModel model;

  GotSlidersState(this.model);
  
}

class ErrorState extends HomeSliderState {
  final String message;

  ErrorState(this.message);
}
