import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';
import 'package:saas/features/statics/data/static_pages_repositories.dart';

part 'home_news_state.dart';

class HomeNewsCubit extends Cubit<HomeNewsState> {
  final StaticPagesRepositories _staticPagesRepositories;
  HomeNewsCubit(this._staticPagesRepositories)
      : super(HomeNewsInitial());
  Future<void> getNews() async {
    emit(LoadingState());
    final either = await _staticPagesRepositories.getHomeNews();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotNewsState(data));
    });
  }
}
