import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/statics/data/static_pages_model.dart';
import 'package:saas/features/statics/data/static_pages_repositories.dart';

part 'home_slider_state.dart';

class HomeSliderCubit extends Cubit<HomeSliderState> {
  final StaticPagesRepositories _staticPagesRepositories;
  HomeSliderCubit(this._staticPagesRepositories) : super(HomeSliderInitial());
  Future<void> getSlider() async {
    emit(LoadingState());
    final either = await _staticPagesRepositories.getHomeSlider();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotSlidersState(data));
    });
  }
}
