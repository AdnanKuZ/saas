part of 'home_news_cubit.dart';

abstract class HomeNewsState {
  const HomeNewsState();
}

class HomeNewsInitial extends HomeNewsState {}

class LoadingState extends HomeNewsState {}

class GotNewsState extends HomeNewsState {
  final StaticPagesModel model;

  GotNewsState(this.model);
  
}

class ErrorState extends HomeNewsState {
  final String message;

  ErrorState(this.message);
}
