import 'dart:convert';

NotificationModel notificationModelFromJson(String str) =>
    NotificationModel.fromJson(json.decode(str)["data"]);

String notificationModelToJson(NotificationModel data) =>
    json.encode(data.toJson());

List<NotificationModel> notificationListModelFromJson(String str) =>
    List<NotificationModel>.from(
        json.decode(str)["data"].map((x) => NotificationModel.fromJson(x)));

String notificationListModelToJson(List<NotificationModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
    
List<NotificationModel> getReadNotifications(
        List<NotificationModel> notificationList) =>
    notificationList.where((notification) => notification.read == 1).toList();

List<NotificationModel> getUnreadNotifications(
        List<NotificationModel> notificationList) =>
    notificationList.where((notification) => notification.read == 0).toList();

class NotificationModel {
  int id;
  int userId;
  String type;
  String title;
  String body;
  int read;
  DateTime createdAt;
  DateTime updatedAt;

  NotificationModel({
    required this.id,
    required this.userId,
    required this.type,
    required this.title,
    required this.body,
    required this.read,
    required this.createdAt,
    required this.updatedAt,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        id: json["id"],
        userId: json["user_id"],
        type: json["type"],
        title: json["title"],
        body: json["body"],
        read: json["read"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "type": type,
        "title": title,
        "body": body,
        "read": read,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
