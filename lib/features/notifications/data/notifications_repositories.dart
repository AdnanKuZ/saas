import 'package:dartz/dartz.dart';
import 'package:saas/core/error/failures.dart';
import 'package:saas/core/network/repositories.dart';
import 'package:saas/features/notifications/data/notification_model.dart';
import 'package:saas/features/notifications/data/notifications_datasource.dart';

class NotificationsRepositories {
  final NotificationsDataSource _notificationsDataSource;
  NotificationsRepositories(this._notificationsDataSource);
  Future<Either<Failure, List<NotificationModel>>> getNotifications() async =>
      await repository(
          () async => await _notificationsDataSource.getNotifications());

  Future<Either<Failure, void>> updateNotification(int id) async =>
      await repository(
          () async => await _notificationsDataSource.updateNotification(id));
}
