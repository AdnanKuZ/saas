import 'dart:convert';

import 'package:http/http.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/network/data_source.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/notifications/data/notification_model.dart';

class NotificationsDataSource {
  final Client _client;
  NotificationsDataSource(this._client);
  Future<List<NotificationModel>> getNotifications() async {
    return dataSource(
        () => _client.get(
              Uri.parse(notificationsUrl),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer ${DI.userService.getUser()?.token}"
              },
            ),
        model: notificationListModelFromJson);
  }

  Future<void> updateNotification(int id) async {
    return dataSource(() => _client.post(
          Uri.parse(updateNotificationUrl(id)),
          body: json.encode({"read": 1}),
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer ${DI.userService.getUser()?.token}"
          },
        ));
  }
}
