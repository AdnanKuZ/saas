part of 'notifications_cubit.dart';

abstract class NotificationsState {
  const NotificationsState();
}

class NotificationsInitial extends NotificationsState {}

class LoadingState extends NotificationsState {}

class GotNotificationsState extends NotificationsState {
  final List<NotificationModel> notifications;

  GotNotificationsState(this.notifications);
}
class ErrorState extends NotificationsState {
  final String message;

  ErrorState(this.message);
}
