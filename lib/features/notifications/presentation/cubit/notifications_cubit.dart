import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/notifications/data/notification_model.dart';
import 'package:saas/features/notifications/data/notifications_repositories.dart';

part 'notifications_state.dart';

class NotificationsCubit extends Cubit<NotificationsState> {
  final NotificationsRepositories _notificationsRepositories;
  NotificationsCubit(this._notificationsRepositories)
      : super(NotificationsInitial());

  Future<void> getNotifications() async {
    emit(LoadingState());
    final either = await _notificationsRepositories.getNotifications();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      print("NotificationsLength:${data.length}");
      print("readNotificationsLength:${getReadNotifications(data).length}");
      print("unreadNotificationsLength:${getUnreadNotifications(data).length}");

      emit(GotNotificationsState(data));
    });
  }
}
