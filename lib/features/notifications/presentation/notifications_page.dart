import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";
import "package:saas/app/extensions/date_time_helper.dart";
import "package:saas/app/widgets/return_appbar.dart";
import "package:saas/features/notifications/data/notification_model.dart";
import "package:saas/features/notifications/presentation/cubit/notifications_cubit.dart";
import "package:saas/features/notifications/presentation/widgets/notification_widget.dart";

class NotificationsPage extends StatelessWidget {
  const NotificationsPage(this.notifications, {super.key});
  final List<NotificationModel> notifications;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        BlocProvider.of<NotificationsCubit>(context).getNotifications();
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SizedBox(
            height: MediaQuery.sizeOf(context).height,
            child: SingleChildScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 18),
                CustomAppBarWithReturn(
                  title: "Notifications",
                  redTitle: true,
                  onReturn: () => BlocProvider.of<NotificationsCubit>(context)
                      .getNotifications(),
                ),
                const SizedBox(height: 40),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 22),
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: notifications.length,
                    itemBuilder: (context, index) => NotificationWidget(
                      title: notifications[index].title,
                      body: notifications[index].body,
                      isRead: notifications[index].read == 0,
                      date: notifications[index].createdAt.formattedDateandTime,
                    ),
                    separatorBuilder: (context, index) => const Divider(
                      height: 30,
                    ),
                  ),
                ),
                const SizedBox(height: 30),
              ],
            )),
          ),
        ),
      ),
    );
  }
}
