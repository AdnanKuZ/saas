import 'package:flutter/material.dart';
import 'package:saas/app/constants/text_styles.dart';

class NotificationWidget extends StatelessWidget {
  const NotificationWidget({
    super.key,
    this.isBluePin = true,
    this.isRead = true,
    required this.title,
    required this.body,
    required this.date,
  });
  final bool isBluePin;
  final bool isRead;
  final String title;
  final String body;
  final String date;
  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.centerRight,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Image.asset(isBluePin
                  ? "assets/icons/notification_pin_blue.png"
                  : "assets/icons/notification_pin_red.png"),
            ),
            const SizedBox(
              width: 9,
            ),
            Expanded(
              flex: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(title,
                      style: TextStyles.notificationWidgetTitleTextStyle),
                  const SizedBox(height: 2),
                  Text(body,
                      style: TextStyles.notificationWidgetTitleTextStyle),
                  const SizedBox(height: 16),
                  Row(
                    children: [
                      SizedBox(
                          width: 14,
                          child: Image.asset(
                              "assets/icons/notification_clock.png")),
                      const SizedBox(width: 6),
                      Text(date,
                          style: TextStyles.notificationWidgetDateTextStyle),
                    ],
                  )
                ],
              ),
            ),
            Expanded(child: Container())
          ],
        ),
        Container(
            width: 10,
            height: 10,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isRead ? const Color(0xFFFF6969) : null),
          ),
      ],
    );
  }
}
