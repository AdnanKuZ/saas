import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:saas/app/di.dart';
import 'package:saas/features/notifications/data/notification_model.dart';
import 'package:saas/features/notifications/data/notifications_datasource.dart';
import 'package:saas/features/notifications/presentation/cubit/notifications_cubit.dart';
import 'package:badges/badges.dart' as badges;
import 'package:saas/features/notifications/presentation/notifications_page.dart';

class NotificationsBellWidget extends StatelessWidget {
  const NotificationsBellWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, top: 0),
      child: BlocConsumer<NotificationsCubit, NotificationsState>(
          listener: (context, state) {
        if (state is GotNotificationsState) {
          if (!DI.pusherService.isConnected) {
            DI.pusherService.connect();
          } //we use this here so we can make sure the session is valid and the pusher connection won't connect if it isn't
        }
      }, builder: (context, state) {
        if (state is GotNotificationsState) {
          List<NotificationModel> notifications =
              state.notifications.reversed.toList();
          List<NotificationModel> unReadNotifications =
              getUnreadNotifications(state.notifications);

          return badges.Badge(
            showBadge: unReadNotifications.isNotEmpty,
            onTap: () {
              updateReadStatusOnNotifications(unReadNotifications, context);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => NotificationsPage(notifications)));
            },
            badgeContent: Text(unReadNotifications.length.toString()),
            child: InkWell(
              onTap: () {
                updateReadStatusOnNotifications(unReadNotifications, context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => NotificationsPage(notifications)));
              },
              child: const Icon(
                Icons.notifications_none_outlined,
                color: Color(0xFF333333),
                size: 30,
              ),
            ),
          );
        } else if (state is LoadingState) {
          return InkWell(
            onTap: () {
              Fluttertoast.showToast(msg: "Loading Notifications");
            },
            child: const Icon(
              Icons.notifications_none_outlined,
              color: Color(0xFF333333),
              size: 30,
            ),
          );
        } else {
          return InkWell(
            onTap: () {
              BlocProvider.of<NotificationsCubit>(context).getNotifications();
              Fluttertoast.showToast(msg: "Notifications Error. Retrying!");
            },
            child: const Icon(
              Icons.notifications_none_outlined,
              color: Color(0xFF333333),
              size: 30,
            ),
          );
        }
      }),
    );
  }

  void updateReadStatusOnNotifications(
      List<NotificationModel> unReadNotifications, BuildContext context) {
    if (unReadNotifications.isNotEmpty) {
      for (NotificationModel notification in unReadNotifications) {
        DI.di<NotificationsDataSource>().updateNotification(notification.id);
      }
    }
  }
}
