import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/widgets/error_occured_widget.dart';
import 'package:saas/features/authentication/data/user_model.dart';
import 'package:saas/features/profile/presentation/cubit/profile_cubit.dart';
import 'package:saas/features/profile/presentation/widgets/profile_page_topsection_widget.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: RefreshIndicator(
          onRefresh: () => BlocProvider.of<ProfileCubit>(context).getCustomer(),
          child: SingleChildScrollView(
              child: SizedBox(
            width: MediaQuery.sizeOf(context).width,
            child: BlocConsumer<ProfileCubit, ProfileState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is LoadingState) {
                    return SizedBox(
                      height: MediaQuery.sizeOf(context).height * 0.9,
                      width: MediaQuery.sizeOf(context).width * 0.9,
                      child: const Center(child: CircularProgressIndicator()),
                    );
                  }
                  if (state is GotCustomerState) {
                    UserModel customer = state.customer;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 36),
                        Container(
                          width: 160,
                          height: 160,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: AppColors.mainblue,
                              image: DecorationImage(
                                  image: AssetImage("assets/images/avatar.png"),
                                  fit: BoxFit.cover)),
                        ),
                        const SizedBox(height: 18),
                        Text(customer.user.name,
                            style: TextStyles.textFieldTextStyle),
                        const SizedBox(height: 8),
                        Text(customer.user.mobile ?? "",
                            style: TextStyles.resendCodeTextStyle),
                        const SizedBox(height: 18),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 18),
                          child: Column(
                            children: [
                              ProfileTopSection(customer),
                              const SizedBox(height: 26),
                              userInfoContainer(
                                  context,
                                  "assets/icons/profile.png",
                                  "Name",
                                  customer.user.name),
                              const SizedBox(height: 26),
                              userInfoContainer(
                                  context,
                                  "assets/icons/phone.png",
                                  "Mobile",
                                  customer.user.mobile ?? ""),
                              const SizedBox(height: 26),
                              userInfoContainer(
                                  context,
                                  "assets/icons/mail.png",
                                  "Email",
                                  customer.user.email),
                              const SizedBox(height: 64),
                            ],
                          ),
                        ),
                      ],
                    );
                  }
                  return SizedBox(
                    height: MediaQuery.sizeOf(context).height * 0.9,
                    width: MediaQuery.sizeOf(context).width * 0.9,
                    child: Center(
                      child: ErrorOccuredTextWidget(
                        errorType: ErrorType.server,
                        fun: () => BlocProvider.of<ProfileCubit>(context)
                            .getCustomer(),
                      ),
                    ),
                  );
                }),
          )),
        ));
  }

  Widget userInfoContainer(
          BuildContext context, String image, String title, String value) =>
      SizedBox(
        width: MediaQuery.sizeOf(context).width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title, style: TextStyles.resendCodeTextStyle),
            const SizedBox(height: 18),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                children: [
                  SizedBox(
                    width: 24,
                    child: Image.asset(image),
                  ),
                  const SizedBox(width: 30),
                  Text(
                    value,
                    style: const TextStyle(
                      color: AppColors.textColor,
                      fontFamily: "Montserrat",
                      fontSize: 18,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
}
