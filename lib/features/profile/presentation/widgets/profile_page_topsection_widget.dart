import 'package:flutter/material.dart';
import 'package:saas/app/constants/colors.dart';
import 'package:saas/app/constants/text_styles.dart';
import 'package:saas/app/di.dart';
import 'package:saas/features/authentication/data/user_model.dart';

class ProfileTopSection extends StatelessWidget {
  const ProfileTopSection(this.customer, {super.key});
  final UserModel customer;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _container(
            Image.asset("assets/icons/doc-file.png"), "My Trips", "2", context),
        // const SizedBox(width: 8),
        // Expanded(
        //     child: _container(
        //         Image.asset("assets/icons/bell.png"), "Notifications", "34")),
      ],
    );
  }

  Widget _container(
          Widget image, String title, String value, BuildContext context) =>
      GestureDetector(
        onTap: () {
          DI.mainService.pageController.jumpToTab(1);
        },
        child: Container(
          height: 80,
          width: MediaQuery.sizeOf(context).width * .5,
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
              color: AppColors.mainblue,
              borderRadius: BorderRadius.circular(16)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              image,
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyles.profileTilesTextStyle,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    value,
                    style: TextStyles.drawerTilesTextStyle,
                  ),
                ],
              )
            ],
          ),
        ),
      );
}
