part of 'profile_cubit.dart';

abstract class ProfileState {
  const ProfileState();
}

class ProfileInitial extends ProfileState {}

class LoadingState extends ProfileState {}

class GotCustomerState extends ProfileState {
  final UserModel customer;

  GotCustomerState(this.customer);
  
}

class ErrorState extends ProfileState {
  final String message;

  ErrorState(this.message);
}
