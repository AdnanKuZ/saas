import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:saas/core/funcs.dart';
import 'package:saas/features/authentication/data/user_model.dart';
import 'package:saas/features/profile/data/profile_repositories.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  final ProfileRepositories _profileRepositories;
  ProfileCubit(this._profileRepositories)
      : super(ProfileInitial());
  Future<void> getCustomer() async {
    emit(LoadingState());
    final either = await _profileRepositories.getCustomer();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotCustomerState(data));
    });
  }
}
