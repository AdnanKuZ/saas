import 'package:http/http.dart';
import 'package:saas/app/di.dart';
import 'package:saas/core/network/data_source.dart';
import 'package:saas/core/network/urls.dart';
import 'package:saas/features/authentication/data/user_model.dart';

class ProfileDataSource {
  final Client _client;
  ProfileDataSource(this._client);
  Future<UserModel> getCustomer() async {
    return dataSource(
      () => _client.get(
        Uri.parse(customerUrl),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${DI.userService.getUser()?.token}"
        },
      ),
      model: userModelFromJson,
      isAuth: true
    );
  }
}
