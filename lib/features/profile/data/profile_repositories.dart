import 'package:dartz/dartz.dart';
import 'package:saas/core/error/failures.dart';
import 'package:saas/core/network/repositories.dart';
import 'package:saas/features/authentication/data/user_model.dart';
import 'package:saas/features/profile/data/profile_datasource.dart';

class ProfileRepositories {
  final ProfileDataSource _profileDataSource;
  ProfileRepositories(this._profileDataSource);
  Future<Either<Failure, UserModel>> getCustomer() async =>
      await repository(() async => await _profileDataSource.getCustomer());
}
