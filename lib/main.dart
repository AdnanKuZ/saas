// import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:saas/app/app.dart';

void main() async {
  await SaasApp.init();
  // await DI.notificationService.initializeNotificationService();
  runApp(const SaasApp());
  
  // runApp(EasyLocalization(
  //     supportedLocales: const [Locale('en', 'US'), Locale('ar', 'SA')],
  //     path: 'assets/translations',
  //     fallbackLocale: const Locale('en', 'US'),
  //     child: const SaasApp()));
}


